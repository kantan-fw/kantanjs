## KantanFW - kantanjs 

## Preamble

A simple framework/boilerplate for handling some stuff you always need if you build a webapp like filters,sort,inputs,forms, dialogs and so on.

## Requirements

### Javascript
- jQuery v3.2.0 https://jquery.com/
- Bootstrap 4.0 https://getbootstrap.com
- Handlebars http://handlebarsjs.com/
- Scroll to https://github.com/flesler/jquery.scrollTo

### Javascript for Forms
- Tiny MCE https://www.tinymce.com/
- Input Mask http://github.com/RobinHerbots/jquery.inputmask
- Moment https://momentjs.com/
- Flatpickr https://github.com/chmln/flatpickr
- Typeahead https://github.com/bassjobsen/Bootstrap-3-Typeahead
- Dropzone (modified!) http://www.dropzonejs.com/

### CSS
- Bootstrap 4.0 https://getbootstrap.com
- Fontawesome http://fontawesome.io/icons/
- Flag Icons https://github.com/lipis/flag-icon-css (optional)

## Tested on
IE11, MS Edge, Chrome, Firefox, Android 5+

## Install & Use

Demos and documentation: http://www.kantan-fw.com
example Files are Included in the package

## Dropzone.js requirement

In Code Line 606 of Dropzone.js i added following Code:

```javascript
 //Workaround for hiddenFileInput to append to given Object by Ezra Neuwirth 2017                       
 if(typeof(_this.options.hiddenInputContainer) === "object")
{            	
	_this.options.hiddenInputContainer.append(_this.hiddenFileInput);	
}else
{
	document.querySelector(_this.options.hiddenInputContainer).appendChild(_this.hiddenFileInput);	
}
```