## KantanFW - kantanjs 

## Preamble

Small help file to setup a demo, i won't add all the external resources inside this package so you have to download them yourself.

## Requirements

See readme.md from the Package

## Install & Use

add one of the following html files inside the body tag (holders.html or inputs.html)
of a newly created index.html file
Download and add  the required JS and CSS Files from their sources to the head
and also following files:

```html
<link rel="stylesheet" href="demo.css" type="text/css" media="screen,print" />	
<script type="text/javascript" src="kantan.min.js"></script>
<script type="text/javascript" src="demo.js"></script>
```

It should look like this now:
http://www.kantan-fw.com/examples/holders/
http://www.kantan-fw.com/examples/inputs/

## Dropzone.js requirement

In Code Line 606 of Dropzone.js i added following Code:

```javascript
 //Workaround for hiddenFileInput to append to given Object by Ezra Neuwirth 2017                       
 if(typeof(_this.options.hiddenInputContainer) === "object")
{            	
	_this.options.hiddenInputContainer.append(_this.hiddenFileInput);	
}else
{
	document.querySelector(_this.options.hiddenInputContainer).appendChild(_this.hiddenFileInput);	
}
```