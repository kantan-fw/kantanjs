/**
 * @preserve 
 * demo.js
 * http://www.kantan-fw.com
 * This is the demo File for the Kantan Web Framework
 * 
 */

var kantanDummyData = {"list":[{"id":1,"title":"Pellentesque habitant morbi tristique","email":"dummy-0-email@dummy.com","role":"User","type":"Article","visible":true,"image":{"thumb":"\/resource\/app\/img\/example-image-1.jpg","big":"\/resource\/app\/img\/example-image-1-1900.jpg","width":1900,"height":1425}},{"id":2,"title":"Pellentesque habitant morbi tristique senectus et netus et malesuada fames","email":"placerat-1-email@eleifend.com","role":"Admin","type":"Date","visible":false,"image":{"thumb":"\/resource\/app\/img\/wide_example-image-2.jpg","big":"\/resource\/app\/img\/example-image-2-1900.jpg"}},{"id":3,"title":"Pellentesque habitant morbi tristique","email":"lorem-2-email@ipsum.com","role":"User","type":"Page","visible":true,"image":{"thumb":"\/resource\/app\/img\/example-image-4.jpg","big":"\/resource\/app\/img\/example-image-4-1900.jpg"}},{"id":4,"title":"Pellentesque habitant morbi tristique senectus et netus et malesuada fames","email":"placerat-3-email@eleifend.com","role":"Admin","type":"Date","visible":false,"image":{"thumb":"\/resource\/app\/img\/wide_example-image-2.jpg","big":"\/resource\/app\/img\/example-image-2-1900.jpg"}},{"id":5,"title":"Vestibulum tortor quam","email":"tortor-4-email@quam.com","role":"Moderator","type":"Comment","visible":true,"image":{"thumb":"\/resource\/app\/img\/example-image-3.jpg","big":"\/resource\/app\/img\/example-image-3-1900.jpg"}},{"id":6,"title":"Pellentesque habitant morbi tristique senectus et netus et malesuada fames","email":"placerat-5-email@eleifend.com","role":"Admin","type":"Date","visible":false,"image":{"thumb":"\/resource\/app\/img\/wide_example-image-2.jpg","big":"\/resource\/app\/img\/example-image-2-1900.jpg"}},{"id":7,"title":"Vestibulum tortor quam","email":"tortor-6-email@quam.com","role":"Moderator","type":"Comment","visible":true,"image":{"thumb":"\/resource\/app\/img\/example-image-3.jpg","big":"\/resource\/app\/img\/example-image-3-1900.jpg"}},{"id":8,"title":"Pellentesque habitant morbi tristique senectus et netus et malesuada fames","email":"placerat-7-email@eleifend.com","role":"Admin","type":"Date","visible":false,"image":{"thumb":"\/resource\/app\/img\/wide_example-image-2.jpg","big":"\/resource\/app\/img\/example-image-2-1900.jpg"}},{"id":9,"title":"Pellentesque habitant morbi tristique","email":"dummy-8-email@dummy.com","role":"User","type":"Article","visible":true,"image":{"thumb":"\/resource\/app\/img\/example-image-1.jpg","big":"\/resource\/app\/img\/example-image-1-1900.jpg","width":1900,"height":1425}},{"id":10,"title":"Pellentesque habitant morbi tristique senectus et netus et malesuada fames","email":"placerat-9-email@eleifend.com","role":"Admin","type":"Date","visible":false,"image":{"thumb":"\/resource\/app\/img\/wide_example-image-2.jpg","big":"\/resource\/app\/img\/example-image-2-1900.jpg"}},{"id":11,"title":"Pellentesque habitant morbi tristique","email":"lorem-10-email@ipsum.com","role":"User","type":"Page","visible":true,"image":{"thumb":"\/resource\/app\/img\/example-image-4.jpg","big":"\/resource\/app\/img\/example-image-4-1900.jpg"}},{"id":12,"title":"Pellentesque habitant morbi tristique senectus et netus et malesuada fames","email":"placerat-11-email@eleifend.com","role":"Admin","type":"Date","visible":false,"image":{"thumb":"\/resource\/app\/img\/wide_example-image-2.jpg","big":"\/resource\/app\/img\/example-image-2-1900.jpg"}},{"id":13,"title":"Vestibulum tortor quam","email":"tortor-12-email@quam.com","role":"Moderator","type":"Comment","visible":true,"image":{"thumb":"\/resource\/app\/img\/example-image-3.jpg","big":"\/resource\/app\/img\/example-image-3-1900.jpg"}},{"id":14,"title":"Pellentesque habitant morbi tristique senectus et netus et malesuada fames","email":"placerat-13-email@eleifend.com","role":"Admin","type":"Date","visible":false,"image":{"thumb":"\/resource\/app\/img\/wide_example-image-2.jpg","big":"\/resource\/app\/img\/example-image-2-1900.jpg"}},{"id":15,"title":"Vestibulum tortor quam","email":"tortor-14-email@quam.com","role":"Moderator","type":"Comment","visible":true,"image":{"thumb":"\/resource\/app\/img\/example-image-3.jpg","big":"\/resource\/app\/img\/example-image-3-1900.jpg"}},{"id":16,"title":"Pellentesque habitant morbi tristique senectus et netus et malesuada fames","email":"placerat-15-email@eleifend.com","role":"Admin","type":"Date","visible":false,"image":{"thumb":"\/resource\/app\/img\/wide_example-image-2.jpg","big":"\/resource\/app\/img\/example-image-2-1900.jpg"}},{"id":17,"title":"Pellentesque habitant morbi tristique","email":"dummy-16-email@dummy.com","role":"User","type":"Article","visible":true,"image":{"thumb":"\/resource\/app\/img\/example-image-1.jpg","big":"\/resource\/app\/img\/example-image-1-1900.jpg","width":1900,"height":1425}},{"id":18,"title":"Pellentesque habitant morbi tristique senectus et netus et malesuada fames","email":"placerat-17-email@eleifend.com","role":"Admin","type":"Date","visible":false,"image":{"thumb":"\/resource\/app\/img\/wide_example-image-2.jpg","big":"\/resource\/app\/img\/example-image-2-1900.jpg"}},{"id":19,"title":"Pellentesque habitant morbi tristique","email":"lorem-18-email@ipsum.com","role":"User","type":"Page","visible":true,"image":{"thumb":"\/resource\/app\/img\/example-image-4.jpg","big":"\/resource\/app\/img\/example-image-4-1900.jpg"}},{"id":20,"title":"Pellentesque habitant morbi tristique senectus et netus et malesuada fames","email":"placerat-19-email@eleifend.com","role":"Admin","type":"Date","visible":false,"image":{"thumb":"\/resource\/app\/img\/wide_example-image-2.jpg","big":"\/resource\/app\/img\/example-image-2-1900.jpg"}},{"id":21,"title":"Vestibulum tortor quam","email":"tortor-20-email@quam.com","role":"Moderator","type":"Comment","visible":true,"image":{"thumb":"\/resource\/app\/img\/example-image-3.jpg","big":"\/resource\/app\/img\/example-image-3-1900.jpg"}},{"id":22,"title":"Pellentesque habitant morbi tristique senectus et netus et malesuada fames","email":"placerat-21-email@eleifend.com","role":"Admin","type":"Date","visible":false,"image":{"thumb":"\/resource\/app\/img\/wide_example-image-2.jpg","big":"\/resource\/app\/img\/example-image-2-1900.jpg"}},{"id":23,"title":"Vestibulum tortor quam","email":"tortor-22-email@quam.com","role":"Moderator","type":"Comment","visible":true,"image":{"thumb":"\/resource\/app\/img\/example-image-3.jpg","big":"\/resource\/app\/img\/example-image-3-1900.jpg"}},{"id":24,"title":"Pellentesque habitant morbi tristique senectus et netus et malesuada fames","email":"placerat-23-email@eleifend.com","role":"Admin","type":"Date","visible":false,"image":{"thumb":"\/resource\/app\/img\/wide_example-image-2.jpg","big":"\/resource\/app\/img\/example-image-2-1900.jpg"}}],"cities":[{"id":1,"name":"Barcelona","fullname":"Spain - Barcelona","country":"Spain"},{"id":2,"name":"Berlin","fullname":"Germany - Berlin","country":"Germany"},{"id":3,"name":"Paris","fullname":"France - Paris","country":"France"},{"id":4,"name":"London","fullname":"UK - London","country":"UK"},{"id":5,"name":"New York","fullname":"USA - New York","country":"USA"},{"id":6,"name":"Stockholm","fullname":"Sweden - Stockholm","country":"Sweden"},{"id":7,"name":"Vienna","fullname":"Austria - Vienna","country":"Austria"}],"programming":{"1":{"id":1,"name":"CSS"},"2":{"id":2,"name":"C"},"3":{"id":3,"name":"C++"},"4":{"id":4,"name":"Java"},"5":{"id":5,"name":"Javascript"},"6":{"id":6,"name":"PHP","description":"\u003Cp\u003EPHP (recursive acronym for PHP: Hypertext Preprocessor) is a widely-used open source general-purpose scripting language that is especially suited for web development and can be embedded into HTML.\u003C\/p\u003E"},"7":{"id":7,"name":"Python"},"8":{"id":8,"name":"Mysql"}},"browsers":{"1":{"id":1,"name":"Chrome","icon":"fa-chrome"},"2":{"id":2,"name":"Firefox","icon":"fa-firefox"},"3":{"id":3,"name":"Internet Explorer","icon":"fa-internet-explorer"},"4":{"id":4,"name":"Opera","icon":"fa-opera"},"5":{"id":5,"name":"Safari","icon":"fa-safari"}},"selected":{"browsers":["1","3"],"programming":["1","4"],"users":["4","6"]},"selected_users":{"4":{"id":4,"name":"Patricia Lebsack","email":"Julianne.OConner@kory.org"},"6":{"id":6,"name":"Mrs. Dennis Schulist","email":"Karley_Dach@jasper.info"}},"images":{"example-1":{"file":"\/resource\/app\/img\/example-image-1-1900.jpg","name":"example-image-1","size":"648kb","width":1900,"height":1425,"thumb":"\/resource\/app\/img\/example-image-1.jpg","wide":"\/resource\/app\/img\/wide_example-image-1.jpg","upright":"\/resource\/app\/img\/upright_0000_example-image.jpg"},"example-2":{"file":"\/resource\/app\/img\/example-image-2-1900.jpg","name":"example-image-2","size":"1.27mb","width":1900,"height":1425,"thumb":"\/resource\/app\/img\/example-image-2.jpg","wide":"\/resource\/app\/img\/wide_example-image-2.jpg","upright":"\/resource\/app\/img\/upright_0001_example-image.jpg"},"example-3":{"file":"\/resource\/app\/img\/example-image-3-1900.jpg","name":"example-image-3","size":"1.22mb","width":1900,"height":1425,"thumb":"\/resource\/app\/img\/example-image-3.jpg","wide":"\/resource\/app\/img\/wide_example-image-3.jpg","upright":"\/resource\/app\/img\/upright_0002_example-image.jpg"},"example-4":{"file":"\/resource\/app\/img\/example-image-4-1900.jpg","name":"example-image-4","size":"1.908kb","width":1900,"height":1425,"thumb":"\/resource\/app\/img\/example-image-4.jpg","wide":"\/resource\/app\/img\/wide_example-image-4.jpg","upright":"\/resource\/app\/img\/upright_0003_example-image.jpg"}}};	
$(document).ready(function()
{	
	if($('body').find("[data-kantan=base]").length)
	{	
		//let's create a new Kantan.App Object and give it the holder where the app runs.
		var appKantan = new Kantan.App();		
		appKantan.init(
			$('body').find("[data-kantan=base]")
		);
		
		//the init call for the Kantan.Utilities object 
		appKantan.callUtility().init(
			appKantan.holder
		);
		
		//Let's add some dummy data for the services (comes from the var kantanDummyData)		
		appKantan.callHolders().getHolders(appKantan.holder).each(function()
		{
			var kantanHolder = $(this);			
			var contentHolder = appKantan.callHolders().getContentHolder(kantanHolder);
			var demoDefaultParameters = false;		
			var inputData = false;
			
			switch(kantanHolder.attr("data-service"))
			{
				case "demo-inputs":
					kantanHolder.find("[data-kantan-field=adress-city]").data("kantanChange",function(kantanCallback)
					{
						appKantan.callHolders().getHolder(kantanCallback.inputHolder).find("[name=adress-country]").val(
							kantanCallback.data.country
						);
					});
					
					inputData = new Object;					
					inputData['browser-multiple'] = kantanDummyData.selected.browsers;		
				break;				
			}
			
			//If you wan't to define defaultParameters via an javascript object this shows you how you could init them
			if(typeof(demoDefaultParameters) === "object")
			{							
				appKantan.callHolders().setDefaults(
					kantanHolder,
					demoDefaultParameters
				);			
				appKantan.callHolders().setData(
					kantanHolder,
					demoDefaultParameters
				);							
			}else
			{
				//if the parameters are set via data-parameters attribute
				if(typeof(kantanHolder.data("parameters")) === "object")
				{
					appKantan.callHolders().setData(
						kantanHolder,
						kantanHolder.data("parameters")
					);
				}
			}
			
			//We call the Kantan.Inputs.Handler and set the Inputs inside the app holder
			appKantan.callInputs().set(
				kantanHolder,
				inputData
			);		
		});
		
		//We call the Kantan.Services.Handler() which initializes on click event for the elements with the data attribute [data-kantan=function]
		//the callback function is for filter and sort Handlers
		appKantan.callHolders().init(
			appKantan.holder,
			function(kantanHolder,button)
			{
				var _self = this;
				var module_data = appKantan.callHolders().getAllData(kantanHolder);
				
				//lets clear the marks if there are some set	
				appKantan.callHolders().callMark().clearMarks(
					kantanHolder,
					module_data
				);	
				
				kantanHolder.find("[data-kantan=result]:first").removeClass(appKantan.css_classes.hidden);
				kantanHolder.find("[data-kantan=result]:first").html(
					JSON.stringify(module_data)
				);		
				
				// For Demo purposes only
				//console.log(module_data);
				
				setTimeout(function()
				{
					appKantan.callUtility().callLoading().complete(kantanHolder);	
					appKantan.callUtility().callLoading().complete(button);	 
				}, 300);			
			}
		);
							
		//We call the Kantan.Utilities.Forms and set the forms for the current app
		appKantan.callUtility().callForms().init(
			appKantan.holder,
			function(kantanHolder,formHolder,form_event)
			{	
				//lets change the Validation from the form			
				appKantan.callUtility().callForms().changeValidation(formHolder,true);		
				
				// For Demo purposes only
				var form_has_errors = false;		
				var formValues = appKantan.callUtility().callForms().getValues(formHolder);				
				var formErrors = {};		
				
				switch(kantanHolder.attr("data-service"))
				{
					case "demo-inputs":
						if(typeof(formValues.birthday) !== "undefined")
						{
							var date = moment(formValues.birthday, "YYYY-MM-DD");
							if(!date.isValid())
							{
								form_has_errors = true;				
								formErrors['birthday'] = {
									date : "<p>Please enter a valid date</p>",
									format : "<p>The correct Format is YYYY-MM-DD</p>"
								};				
							}			
						}
						if(typeof(formValues['language_fields[de][telephone]']) === "undefined" || formValues['language_fields[de][telephone]'].length == 0)
						{
							form_has_errors = true;
							formErrors['telephone-de'] = {
								empty : "<p>You have to enter a telephone number</p>"					
							};		
						}
					break;
				}
				
				//lets clear the result from empty values
				for(form_value_key in formValues)
				{
					if(formValues[form_value_key].length == 0)
					{
						delete(formValues[form_value_key]);
					}					
				}
				
				//Let's remove the image-files so that you wont't get a base64 encoded image in the result alert
				// i will only remove the first 3				
				if(typeof(formValues['image-file[1]']) !== "undefined")
				{
					console.log(formValues['image-file[1]']);
					delete formValues['image-file[1]'];			
				}
				if(typeof(formValues['image-file[2]']) !== "undefined")
				{
					console.log(formValues['image-file[2]']);
					delete formValues['image-file[2]'];			
				}
				if(typeof(formValues['image-file[3]']) !== "undefined")
				{
					console.log(formValues['image-file[3]']);
					delete formValues['image-file[3]'];			
				}
				if(typeof(formValues['language_fields[en][attachment-file]']) !== "undefined")
				{
					console.log(formValues['language_fields[en][attachment-file]']);
					delete formValues['language_fields[en][attachment-file]'];	
				}
				
				kantanHolder.find("[data-kantan=result]:first").removeClass(appKantan.css_classes.hidden);
				kantanHolder.find("[data-kantan=result]:first").html(
					JSON.stringify(formValues)
				);	
				
				setTimeout(function()
				{
					if(form_has_errors)
					{
						appKantan.callUtility().callForms().setErrors(kantanHolder,formHolder,formErrors);
					}else
					{
						appKantan.callUtility().callForms().setComplete(kantanHolder,formHolder);
					}			
				}, 300);				
			}
		);
		
		//after everything is set lets remove the loading from the app
		appKantan.callUtility().callLoading().complete(appKantan.holder);	
	}
});