/*!
* kantanjs Bundle
* https://gitlab.com/kantan-fw/kantanjs/
* Copyright (c) 2017 Ezra Neuwirth
* Licensed under the MIT license (http://www.opensource.org/licenses/mit-license.php)
* @author : Ezra Neuwirth
* @version : 1.0.0
* 
* @preserve
* Kantan.Holders.Handler
* Creates functions for data-kantan-holder elements
* requires jquery
* for filter Kantan.Holders.Filters
 * for sort Kantan.Holders.Sort
 * for mark Kantan.Holders.Mark
*/

Kantan.Holders.Handler = function(currentApp)
{	
	this.app = currentApp;
	
	this.parameters_key ="parameters";		
	this.getDataKey = function()
	{
		return this.parameters_key;
	}
	this.function_key ="data-kantan-holder-function";
	this.getFunctionKey = function(braces)
	{
		if(braces)
		{
			return "[" + this.function_key + "]";
		}
		return this.function_key;
	}
	this.createFunctionKey = function(name)
	{
		return "[" + this.getFunctionKey() + "=" + name + "]";		
	}
	
	this.objects = {
		mark : new Kantan.Holders.Mark(this.app,this),
		sort : new Kantan.Holders.Sort(this.app,this),
		filters : new Kantan.Holders.Filters(this.app,this),
	}
	
	this.callFilters = function()
	{		
		return this.objects.filters;
	};
	this.callSort = function()
	{		
		return this.objects.sort;
	};
	this.callMark = function()
	{		
		return this.objects.mark;
	};	
};

Kantan.Holders.Handler.prototype = 
{
	init : function(holder,onLoad)
	{		
		var _self = this;
		
		holder.on("click",_self.getFunctionKey(true),function(event)
		{
			var function_holder = $(this);
			var target = $(event.target);
			
			switch(function_holder.attr(_self.function_key))
			{				
				case "filter":
					_self.callFilters().callRun(
						function_holder,
						target,
						onLoad
					);
				break;
				
				case "sort":
					if(!_self.app.callUtility().callLoading().isLoading(function_holder))
					{
						_self.callSort().run(
							function_holder,
							target,
							function(serviceHolder,button)
							{								
								if(typeof(onLoad) === "function")
								{
									onLoad(serviceHolder,button);
								}	
							}
						);
					}
				break;
					
				case "mark":
					_self.callMark().run(
						function_holder,
						target
					);
				break;
				
				case "dialog":
					if(!_self.app.callUtility().callLoading().isLoading(function_holder))
					{
						var serviceHolder = _self.getHolder(function_holder);
						var dialogHolder = serviceHolder.find("[data-kantan-dialog=" + function_holder.attr("data-dialog") + "]");
						
						_self.app.callUtility().callDialogs().show(
							dialogHolder,
							serviceHolder,
							function_holder
						);						
					}				
				break;
				
				case "dialog-close":
					var dialogHolder = _self.getHolder(function_holder);				
					var serviceHolder = _self.getHolder(dialogHolder);
					
					_self.app.callUtility().callDialogs().hide(
						dialogHolder,
						serviceHolder,
						function_holder
					);				
				break;
			}
		});
		
		_self.callFilters().setForms(
			holder,onLoad
		);		
	},
	
	getHolders : function(element)
	{
		return element.find("[data-kantan-holder]");
	},
	
	getHolder : function(element)
	{
		return element.parents("[data-kantan-holder]:first");
	},
	
	getContentHolder : function(element)
	{
		return element.find("[data-kantan-holder-content]:first");
	},
	
	getContentFromParent : function(element)
	{
		return this.getContentHolder(this.getHolder(element));
	},
	
	getResultHolder : function(element)
	{
		return element.find("[data-kantan-holder-result]:first");
	},
	
	getSortHolder : function(element)
	{
		return element.find("[data-kantan-holder-sort]:first");
	},
	
	scrollTo : function(element,scrolltoElement)
	{		
		if(typeof(element.attr("data-kantan-holder")) === "undefined")
		{
			var serviceHolder = this.getHolder(element);	
		}else
		{
			var serviceHolder = element;
		}
		
		var already_scrolled = false;
		var scroll_to = 0;
		if(typeof(scrolltoElement) === "object")
		{
			scroll_to = scrolltoElement;
		}		
		
		if(serviceHolder.find('[data-kantan-holder-scroll]').length)
		{			
			serviceHolder.find('[data-kantan-holder-scroll]').scrollTo(
				scroll_to,
				600			
			);
			already_scrolled = true;
		}
		
		if(!already_scrolled)
		{
			switch(serviceHolder.attr("data-kantan-holder"))
			{
				case "content":
					if(typeof(scrolltoElement) === "object")
					{
						$(window).scrollTo(
							scrolltoElement,
							600						
						);
					}else
					{
						$(window).scrollTo(
							serviceHolder,
							600						
						);			
					}			
				break;
				
				case "dialog":
					serviceHolder.scrollTo(
						scroll_to,
						600						
					);				
				break;
			}			
		}			
	},
	
	getAllData : function(serviceHolder)
	{
		if(typeof(serviceHolder.data(this.parameters_key)) === "object")
		{
			return serviceHolder.data(this.parameters_key);
		}else
		{
			return false;
		}
	},
	
	getDefaults : function(serviceHolder)
	{
		return this.app.callUtility().callData().parseDataAtrribute(
			serviceHolder,
			"data-" + this.parameters_key
		);
	},
	
	setDefaults : function(serviceHolder,data)
	{		
		if(typeof(JSON.stringify) === "function")
		{
			serviceHolder.attr("data-" + this.parameters_key,JSON.stringify(data));					
		}
	},
	
	setData : function(serviceHolder,data,reset_data)
	{
		var _self = this;			
		var existing_data = _self.getAllData(serviceHolder);
		
		if(serviceHolder.find(_self.callFilters().getHolderKey(true)).length)
		{
			data = _self.callFilters().setData(
				serviceHolder,
				data,
				existing_data,
				reset_data
			);			
		}		
		
		if(_self.getSortHolder(serviceHolder).length)
		{
			data = _self.callSort().setData(
				serviceHolder,
				_self.getSortHolder(serviceHolder),
				data,				
				existing_data
			);
		}

		_self.app.callUtility().callData().init(
			serviceHolder,
			{
				data_key : this.parameters_key,
				data : data
			}
		);
	}
};