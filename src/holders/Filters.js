/*!
* kantanjs Bundle
* https://gitlab.com/kantan-fw/kantanjs/
* Copyright (c) 2017 Ezra Neuwirth
* Licensed under the MIT license (http://www.opensource.org/licenses/mit-license.php)
* @author : Ezra Neuwirth
* @version : 1.0.0
* 
* @preserve
* Kantan.Holders.Filters
* Creates  filter Functions for holders
* requires jquery
*/

Kantan.Holders.Filters = function(currentApp,kantanHolders)
{	
	this.app = currentApp;
	this.callHolders = function()
	{		
		return kantanHolders;
	};		
	this.function_key = kantanHolders.createFunctionKey("filter");	
	this.holder_key = "data-kantan-filter-holder";
	this.getHolderKey = function(braces)
	{
		if(braces)
		{
			return "[" + this.holder_key + "]";
		}
		return this.holder_key;
	}
};

Kantan.Holders.Filters.prototype = 
{
	setForms : function(holder,onLoad)
	{
		var _self = this;
		
		holder.on("submit","[data-kantan-filter-form]",function(event)
		{
			var formHolder = $(this);
			var target = $(event.target);
			var function_holder = formHolder.find("[data-filter-submit]");
			
			event.preventDefault();
        	event.stopPropagation();        	
        	
			if(function_holder.length)
			{
				_self.callRun(
					function_holder,
					target,
					onLoad
				);
			}			
		});		
	},
	
	callRun : function(function_holder,target,onLoad)
	{
		var _self = this;
		
		if(!_self.app.callUtility().callLoading().isLoading(function_holder))
		{
			_self.run(
				function_holder,
				target,
				function(kantanHolder,button)
				{	 													
					if(typeof(onLoad) === "function")
					{
						onLoad(kantanHolder,button);
					}																												
				}
			);
		}
	},
	
	run : function(function_holder,target,onComplete)
	{
		var _self = this;
		
		var filterHolder = function_holder.parents( _self.getHolderKey(true) );		
		var kantanHolder = _self.callHolders().getHolder(function_holder);
		var contentHolder =_self.callHolders().getContentHolder(kantanHolder);
		
		_self.app.callUtility().callLoading().loading(kantanHolder);
		_self.app.callUtility().callLoading().loading(function_holder);
		
		var callback_type = false;
		var remove_active = false;
		var set_active = false;
		
		var set_value = false;
		var filter_value = false;
		var remove_value = false;				
				
		var show_clear_all = true;
		var clear_filters = false;		
		var load_data = true;
		
		var filter_type = filterHolder.attr("data-type");		
		if(typeof(function_holder.attr("data-filter-call")) !== "undefined")
		{
			filter_type = function_holder.attr("data-filter-call");
		}		
		if(typeof(filterHolder.attr("data-clear")) !== "undefined" && filterHolder.attr("data-clear") == "false")
		{
			show_clear_all = false;
		}
		
		switch(filter_type)
		{
			case "dropdown":
				_self.app.callInputs().callDropdown().setTitle(filterHolder,function_holder);			
				set_value = true;
				set_active = true;
				remove_active = true;				
			break;
			
			case "single":
				if(function_holder.hasClass(_self.app.css_classes.active))
				{
					remove_value = true;
				}else
				{
					set_value = true;
					set_active = true;
				}				
				remove_active = true;				
			break;
			
			case "single-switch":
				if(!function_holder.hasClass(_self.app.css_classes.active))
				{
					set_value = true;
					set_active = true;
					remove_active = true;					
				}else
				{
					load_data = false;
				}				
			break;
			
			case "paging":
				load_data = false;			
				
				var currentPage = parseInt(_self.app.callUtility().callData().get(
					kantanHolder,
					{
						data_key : _self.callHolders().getDataKey(),
						parameter : filterHolder.attr(_self.getHolderKey())
					}
				));
				
				if(!function_holder.parent().hasClass(_self.app.css_classes.active))
				{										
					switch(function_holder.attr("data-value"))
					{
						case "next":
							var pageValue = currentPage+1;							
						break;
						
						case "prev":
							var pageValue = currentPage-1;						
						break;
						
						default:
							var pageValue = function_holder.attr("data-value");							
						break;
					}
																							
					load_data = _self.setPage(
						kantanHolder,
						filterHolder,
						pageValue,
						load_data
					);					
				}
			break;
			
			case "multiple":				
				var multipleValues = _self.app.callUtility().callData().get(
					kantanHolder,
					{
						data_key : _self.callHolders().getDataKey(),
						parameter : filterHolder.attr(_self.getHolderKey()),
						type : "array"
					}
				);
				
				var multiple_array_action = null;				
				if(function_holder.hasClass(_self.app.css_classes.active))
				{
					function_holder.removeClass(_self.app.css_classes.active);									
					multiple_array_action = "remove";
				}else
				{
					set_active = true;				
					multiple_array_action = "add";
				}
				
				_self.app.callUtility().callData().changeArray(
					multipleValues,
					function_holder.attr("data-value"),
					multiple_array_action
				);		
											
				if(multipleValues.length == 0)
				{
					remove_value = true;						
				}
			break;
			
			case "form":				
				filter_value = _self.app.callUtility().callForms().getValues(filterHolder);
				set_value = true;			
				filterHolder.find("[data-filter-call=clear-form]").removeClass(_self.app.css_classes.hidden);	
			break;
			
			case "clear-form":
				function_holder.addClass(_self.app.css_classes.hidden);						
				remove_value = true;
				
				_self.app.callUtility().callForms().clearForm(filterHolder);				
				_self.app.callInputs().clear(filterHolder);							
			break;
			
			case "clear-all":
				show_clear_all = false;
				clear_filters = true;				
				contentHolder.find("[data-filter-call=clear-all]").addClass(_self.app.css_classes.hidden);			
				contentHolder.find("[data-filter-call=clear-form]").addClass(_self.app.css_classes.hidden);				
				_self.callHolders().scrollTo(kantanHolder);
			break;
		}
		
		if(!filter_value)
		{
			filter_value = function_holder.attr("data-value");
		}		
		
		if(set_value)
		{				
			_self.app.callUtility().callData().add(
				kantanHolder,	
				{
					data_key : _self.callHolders().getDataKey(),					
					parameter : filterHolder.attr(_self.getHolderKey()),
					value : filter_value
				}			
			);						
			callback_type = "addFilter";				
		}
		
		if(remove_value)
		{
			_self.app.callUtility().callData().remove(
				kantanHolder,
				{
					data_key : _self.callHolders().getDataKey(),	
					parameter : filterHolder.attr(_self.getHolderKey())
				}
			);			
			callback_type = "removeFilter";		
		}
		
		if(filter_type == "multiple")
		{
			callback_type = multiple_array_action + "Filter";			
		}
		
		if(callback_type && typeof(filterHolder.data("kantanChange")) === "function")
		{
			filterHolder.data("kantanChange")({
				call : callback_type,				
				kantanObject : _self,
				filterHolder : filterHolder,				
				value : filter_value
			});
		}
		
		if(remove_active)
		{
			filterHolder.find( _self.function_key ).removeClass(_self.app.css_classes.active);
		}
		if(set_active)
		{
			function_holder.addClass(_self.app.css_classes.active);
		}		
				
		if(load_data)
		{						
			if(show_clear_all)
			{
				contentHolder.find("[data-filter-call=clear-all]").removeClass(_self.app.css_classes.hidden);
			}
			
			if(clear_filters)
			{	
				_self.app.callInputs().clear(contentHolder.find("[data-kantan-holder-filters]"));				
				
				_self.callHolders().setData(
					kantanHolder,
					_self.callHolders().getDefaults(kantanHolder),
					true
				);
			}
			
			if(typeof(onComplete) === "function")
			{
				onComplete(kantanHolder,function_holder);
			}
		}else
		{
			_self.app.callUtility().callLoading().complete(kantanHolder);
			_self.app.callUtility().callLoading().complete(function_holder);
		}		
	},
	
	setData : function(kantanHolder,data,existing_data,reset_data)
	{
		var _self = this;		
		
		_self.callHolders().getContentHolder(kantanHolder).find( _self.getHolderKey(true) ).each(function()
		{
			var filterHolder = $(this);			
			
			var filter_value = null;			
			if(typeof(data[filterHolder.attr(_self.getHolderKey())] !== "undefined"))
			{
				filter_value = data[filterHolder.attr(_self.getHolderKey())];	
			}
			
			if(typeof(filterHolder.attr("data-clear") !== "undefined") && filterHolder.attr("data-clear") == "false")
			{				
				if(typeof(existing_data) === "object" && existing_data[filterHolder.attr(_self.getHolderKey())] !== "undefined")
				{
					var existing_value = existing_data[filterHolder.attr(_self.getHolderKey())];					
					if(typeof(existing_value) !== "undefined")
					{
						data[filterHolder.attr(_self.getHolderKey())] = existing_value;
						filter_value = existing_value;
					}
				}
			}
			
			if(typeof(filterHolder.data("kantanChange")) === "function")
			{
				var callback_setcall = false;				
				if(typeof(reset_data) === "undefined" && typeof(filter_value) !== "undefined")
				{
					callback_setcall = "setFilter";
				}
				if(reset_data && typeof(filter_value) === "undefined")
				{
					callback_setcall = "clearFilter";
				}
				if(reset_data && typeof(filter_value) !== "undefined")
				{
					callback_setcall = "setFilter";
				}					
				if(callback_setcall)
				{
					filterHolder.data("kantanChange")({
						call : callback_setcall,				
						kantanObject : _self,
						filterHolder : filterHolder,								
						value : filter_value
					});
				}				
			}
			
			switch(filterHolder.attr("data-type"))
			{
				case "form":				
					_self.app.callUtility().callForms().clearForm(filterHolder);									
				break;
				
				case "paging":
					if(typeof(filter_value) === "undefined")
					{
						filter_value = 1;						
						data[filterHolder.attr(_self.getHolderKey())] = filter_value;						
					}
					_self.setPage(
						kantanHolder,
						filterHolder,
						filter_value
					);
				break;
				
				case "dropdown":
				case "multiple":
				case "single-switch":								
				case "single":
					filterHolder.find( _self.function_key ).removeClass(_self.app.css_classes.active);
					
					if(filterHolder.attr("data-type") == "multiple")
					{
						if(typeof(filter_value) === "object")
						{
							for (filer_counter in filter_value)
							{
								var multiple_value = filter_value[filer_counter];
								_self.setValueActive(
									filterHolder,
									multiple_value
								);								
							}
						}
					}else
					{
						var callback_type = false;
						
						if(typeof(filter_value) !== "undefined")
						{
							_self.setValueActive(
								filterHolder,
								filter_value
							);													
						}
						
						if(filterHolder.attr("data-type") == "dropdown")
						{
							filterHolder.find("[data-toggle=dropdown]").html(
								filterHolder.find("[data-value=" + filter_value + "]").find("[data-dropdown=title]").html()
							);
						}
					}
				break;				
			}						
		});
		
		return data;
	},
	
	setValueActive : function(filterHolder,filter_value)
	{
		var _self = this;		
		filterHolder.find("[data-value=" + filter_value + "]").addClass(_self.app.css_classes.active);
	},
	
	setPage : function(kantanHolder,filterHolder,pageValue,load_data)
	{				
		var _self = this;		
		pageValue = parseInt(pageValue);
		
		filterHolder.find("[data-paging=entry]").removeClass(_self.app.css_classes.active);
		if(pageValue > 0)
		{						
			if(filterHolder.find("[data-value=" + pageValue + "]").length)
			{
				filterHolder.find("[data-value=" + pageValue + "]").parent().addClass(_self.app.css_classes.active);
				
				_self.app.callUtility().callData().add(
					kantanHolder,	
					{
						data_key : _self.callHolders().getDataKey(),					
						parameter : filterHolder.attr(_self.getHolderKey()),
						value : pageValue
					}			
				);	
				
				load_data = true;	
			}
			if(filterHolder.find("[data-value=" + (pageValue+1) + "]").length)
			{
				filterHolder.find("[data-value=next]").parent().removeClass(_self.app.css_classes.disabled);
			}else
			{
				filterHolder.find("[data-value=next]").parent().addClass(_self.app.css_classes.disabled);
			}
			if(filterHolder.find("[data-value=" + (pageValue-1) + "]").length)
			{
				filterHolder.find("[data-value=prev]").parent().removeClass(_self.app.css_classes.disabled);
			}else
			{
				filterHolder.find("[data-value=prev]").parent().addClass(_self.app.css_classes.disabled);
			}
		}	
		
		return load_data;
	}
};
