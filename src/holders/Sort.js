/*!
* kantanjs Bundle
* https://gitlab.com/kantan-fw/kantanjs/
* Copyright (c) 2017 Ezra Neuwirth
* Licensed under the MIT license (http://www.opensource.org/licenses/mit-license.php)
* @author : Ezra Neuwirth
* @version : 1.0.0
* 
* @preserve
* Kantan.Holders.Sort
* Creates function to handle sort buttons
 * requires jquery
*/

Kantan.Holders.Sort = function(currentApp,kantanHolders)
{	
	this.app = currentApp;
	this.callHolders = function()
	{		
		return kantanHolders;
	};	
	this.function_key = kantanHolders.createFunctionKey("sort");
};

Kantan.Holders.Sort.prototype = 
{
	classes: {		
		multiple : "multiple",
		sort_asc : "asc",
		sort_desc : "desc"
	},
	
	run : function(button,target,onComplete)
	{
		var _self = this;
		
		var keep_sort = false;							
		var sort_type = button.attr("data-type");		
		var kantanHolder = _self.callHolders().getHolder(button);
		var sortHolder = _self.callHolders().getSortHolder(kantanHolder);
		
		_self.app.callUtility().callLoading().loading(kantanHolder);
		_self.app.callUtility().callLoading().loading(button);
		
		if(!button.hasClass( _self.app.css_classes.active ))
		{
			keep_sort = true;																							
		}
		
		switch(sort_type)
		{
			case "sort-multiple":			
				button.removeClass( _self.classes.sort_asc ).removeClass( _self.classes.sort_desc );	
								
				if(target.attr("data-sort") != "delete") 
				{
					sortHolder.find("[data-type=sort-single]").each(function()
					{
						var sort_single = $(this);
						if(sort_single.hasClass( _self.app.css_classes.active ))
						{
							sort_single.removeClass( _self.app.css_classes.active );
														
							_self.app.callUtility().callData().remove(
								kantanHolder,	
								{
									data_key : _self.callHolders().getDataKey(),
									key : "sort",
									parameter : sort_single.attr("data-sort-key")
								}							
							);									
						}
					});																																
					if(!button.hasClass( _self.app.css_classes.active ))
					{
						button.addClass( _self.app.css_classes.active );
					}												
				}else
				{
					button.removeClass( _self.app.css_classes.active );											
					keep_sort = true;
				}									
			break;
			
			default:
				_self.resetClasses(sortHolder);				
				button.addClass( _self.app.css_classes.active ).removeClass( _self.classes.sort_asc ).removeClass( _self.classes.sort_desc );	
			break;
		}
		
		if(keep_sort)
		{
			if(button.attr("data-sort") == "asc")
			{														
				button.addClass( _self.classes.sort_asc );										
			}else
			{
				button.addClass( _self.classes.sort_desc );																		
			}	
		}else
		{
			if(button.attr("data-sort") == "asc")
			{
				button.attr("data-sort","desc");	
				button.addClass( _self.classes.sort_desc );										
			}else
			{
				button.attr("data-sort","asc");
				button.addClass( _self.classes.sort_asc );																									
			}		
		}
		
		switch(sort_type)
		{
			case "sort-multiple":				
				_self.app.callUtility().callData().remove(
					kantanHolder,	
					{
						data_key : _self.callHolders().getDataKey(),
						key : "sort",
						parameter : button.attr("data-sort-key")
					}			
				);
																	
				if(button.hasClass( _self.app.css_classes.active ))
				{		
					_self.app.callUtility().callData().add(
						kantanHolder,	
						{
							data_key : _self.callHolders().getDataKey(),
							key : "sort",
							parameter : button.attr("data-sort-key"),
							value : button.attr("data-sort")
						}			
					);					
				}
				_self.setMultiple(
					sortHolder,					
					_self.app.callUtility().callData().getLength(
						kantanHolder,	
						{
							data_key : _self.callHolders().getDataKey(),
							key : "sort"						
						}			
					)					
				);
			break;
			
			default:
				_self.app.callUtility().callData().clear(
					kantanHolder,	
					{
						data_key : _self.callHolders().getDataKey(),
						key : "sort"						
					}			
				);
				
				_self.app.callUtility().callData().add(
					kantanHolder,	
					{
						data_key : _self.callHolders().getDataKey(),
						key : "sort",
						parameter : button.attr("data-sort-key"),
						value : button.attr("data-sort")
					}			
				);								
			break;
		}
		
		if(typeof(onComplete) === "function")
		{
			onComplete(kantanHolder,button);
		}		
	},
	
	resetClasses : function(sortHolder,all)
	{
		var _self = this;		
		sortHolder.find( _self.function_key ).removeClass( _self.app.css_classes.active );
		if(all)
		{			
			sortHolder.find( _self.function_key ).removeClass( _self.classes.sort_asc ).removeClass( _self.classes.sort_desc ).removeClass( _self.classes.multiple );	
		}
	},
	
	setMultiple : function(sortHolder,length)
	{
		var _self = this;		
		if(length > 1)
		{			
			sortHolder.find("[data-type=sort-multiple]").addClass( _self.classes.multiple );
		}else
		{			
			sortHolder.find( _self.function_key ).removeClass( _self.classes.multiple );
		}	
	},
	
	setData : function(kantanHolder,sortHolder,data,existing_data)
	{
		var _self = this;		
		_self.resetClasses(sortHolder,true);
		
		if(typeof(existing_data) !== "undefined" &&typeof(existing_data['sort']) !== "undefined")
		{
			if(typeof(sortHolder.attr("data-clear") !== "undefined") && sortHolder.attr("data-clear") == "keep")
			{
				data['sort'] = existing_data['sort'];	
			}
		}
		
		if(typeof(data['sort']) !== "undefined")
		{			
			for(sort_key in data['sort'])
			{
				var sort_direction = data['sort'][sort_key];				
				var sortButton = sortHolder.find("[data-sort-key=" + sort_key + "]");
								
				if(sortButton.length)
				{
					sortButton.addClass(_self.app.css_classes.active);					
					sortButton.attr("data-sort",sort_direction);
					
					switch(sort_direction)
					{
						case "asc":
							sortButton.addClass( _self.classes.sort_asc );
						break;
						
						case "desc":
							sortButton.addClass( _self.classes.sort_desc );
						break;
					}				
				}				
			}
			_self.setMultiple(
				sortHolder,
				 Object.keys(data['sort']).length
			);
		}
		
		return data;
	}
};