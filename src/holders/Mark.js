/*!
* kantanjs Bundle
* https://gitlab.com/kantan-fw/kantanjs/
* Copyright (c) 2017 Ezra Neuwirth
* Licensed under the MIT license (http://www.opensource.org/licenses/mit-license.php)
* @author : Ezra Neuwirth
* @version : 1.0.0
* 
* @preserve
* Kantan.Holders.Mark
* Creates function to mark elements
 * requires jquery
*/

Kantan.Holders.Mark = function(currentApp,kantanHolders)
{	
	this.app = currentApp;	
	this.callHolders = function()
	{		
		return kantanHolders;
	};	
};

Kantan.Holders.Mark.prototype = 
{
	run : function(function_holder,target)
	{	
		var _self = this;		
		var run_mark = true;
		var kantanHolder = _self.callHolders().getHolder(function_holder);
		
		switch(function_holder.attr("data-mark"))
		{
			case "all":					
				if(function_holder.hasClass(_self.app.css_classes.active))
				{
					function_holder.removeClass(_self.app.css_classes.active);					
				}else
				{
					function_holder.addClass(_self.app.css_classes.active);					
				}
			break;
			
			case "list":
				if(target.prop("localName") == "button" || target.prop("localName") == "a")
				{
					run_mark = false;
				}
			break;
		}
			
		if(run_mark)
		{
			var markedValues = _self.app.callUtility().callData().get(
				kantanHolder,
				{
					data_key : _self.callHolders().getDataKey(),
					parameter : "marked",
					type : "array"
				}
			);
			
			switch(function_holder.attr("data-mark"))
			{
				case "all":					
					_self.callHolders().getResultHolder(kantanHolder).find("[data-mark=list]").each(function()
					{
						var markable_entry = $(this);					
						_self.changeState(
							markable_entry,
							markedValues
						);					
					});
				break;
				
				case "list":
					_self.changeState(
						function_holder,
						markedValues
					);				
				break;						
			}
			
			var marked_counter = markedValues.length;
			if(marked_counter == 0)
			{
				_self.app.callUtility().callData().remove(
					kantanHolder,	
					{
						data_key : _self.callHolders().getDataKey(),
						parameter : "marked"						
					}							
				);				
			}
			_self.setHolders(kantanHolder,marked_counter);		
		}		
	},
	
	changeState : function(markable_entry,markedValues)
	{
		var _self = this;
		
		var stateButton = markable_entry.find("[data-mark=state]");		
		var entryActions = markable_entry.find("[data-kantan-list-menue]");		
			
		if(stateButton.hasClass(_self.app.css_classes.active))
		{
			stateButton.removeClass(_self.app.css_classes.active);
			if(entryActions.length)
			{
				entryActions.removeClass(_self.app.css_classes.active);	
			}			
			
			_self.app.callUtility().callData().changeArray(
				markedValues,
				markable_entry.attr("data-id"),
				"remove"
			);				
		}else
		{
			stateButton.addClass(_self.app.css_classes.active);
			if(entryActions.length)
			{
				entryActions.addClass(_self.app.css_classes.active);	
			}
			
			_self.app.callUtility().callData().changeArray(
				markedValues,
				markable_entry.attr("data-id"),
				"add"
			);
		}			
	},
	
	setHolders : function(kantanHolder,marked_counter)
	{
		var _self = this;
		
		var infoHolder = kantanHolder.find("[data-kantan=list-info]:first");		
		if(infoHolder.length)
		{
			if(marked_counter > 0)
			{
				infoHolder.find("[data-marked=count-holder]").removeClass(_self.app.css_classes.hidden);
				infoHolder.find("[data-marked=count]").html(marked_counter);
			}else
			{
				infoHolder.find("[data-marked=count-holder]").addClass(_self.app.css_classes.hidden);
			}		
		}
		
		var actionHolder = kantanHolder.find("[data-kantan-list-actions]:first");
		if(actionHolder.length)
		{
			if(marked_counter > 0)
			{
				actionHolder.find("[data-marked=button]").removeClass(_self.app.css_classes.disabled);
			}else
			{
				actionHolder.find("[data-marked=button]").addClass(_self.app.css_classes.disabled);
			}
		}
	},
	
	clearMarks : function(kantanHolder,module_data)
	{
		var _self = this;
	
		if(typeof(module_data['marked']) !== "undefined")
		{			
			_self.app.callUtility().callData().remove(
				kantanHolder,	
				{
					data_key : _self.callHolders().getDataKey(),
					parameter : "marked"						
				}							
			);			
		}
		
		kantanHolder.find("[data-mark=all]:first").removeClass(_self.app.css_classes.active);		
		
		var resultHolder = _self.callHolders().getResultHolder(kantanHolder);		
		resultHolder.find("[data-mark=state]").removeClass(_self.app.css_classes.active);
		resultHolder.find("[data-kantan=list-actions]").removeClass(_self.app.css_classes.active);
		
		_self.setHolders(kantanHolder,0);	
	}
};