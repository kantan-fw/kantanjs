/*!
* kantanjs Bundle
* https://gitlab.com/kantan-fw/kantanjs/
* Copyright (c) 2017 Ezra Neuwirth
* Licensed under the MIT license (http://www.opensource.org/licenses/mit-license.php)
* @author : Ezra Neuwirth
* @version : 1.0.0
* 
* @preserve
* Kantan.Inputs.Selects
* sets an option to selected from a select input
* requires jquery
*/

Kantan.Inputs.Selects = function(currentApp)
{	
	this.app = currentApp;
};
Kantan.Inputs.Selects.prototype = 
{
	run : function(holder,inputHolder)
	{		
		var value_to_set = false;		
		if(typeof(inputHolder.data("value")) !== "undefined")
 		{
 			value_to_set = inputHolder.data("value"); 			
 		}		
 		if(value_to_set)
 		{			 			
 			inputHolder.find("option[value='" + value_to_set + "']").attr("selected","selected");
 		}
	}
};