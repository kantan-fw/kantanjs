/*!
* kantanjs Bundle
* https://gitlab.com/kantan-fw/kantanjs/
* Copyright (c) 2017 Ezra Neuwirth
* Licensed under the MIT license (http://www.opensource.org/licenses/mit-license.php)
* @author : Ezra Neuwirth
* @version : 1.0.0
* 
* @preserve
* Kantan.Inputs.Languages
* sets the input holder which are multi language enabled to the given value
* requires jquery
*/

Kantan.Inputs.Languages = function(currentApp)
{	
	this.app = currentApp;	
	this.getKey = function()
	{
		return "language_fields";
	};	
	this.getAttribute = function()
	{
		return "data-kantan-language";
	};	
};

Kantan.Inputs.Languages.prototype = 
{	
	set : function(holder,value)
	{
		var _self = this;		
		holder.find("[" + _self.getAttribute() + "]").addClass(_self.app.css_classes.hidden);		
		holder.find("[" + _self.getAttribute() + "=" + value + "]").removeClass(_self.app.css_classes.hidden);		
	}
};