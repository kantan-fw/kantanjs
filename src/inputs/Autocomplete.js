/*!
* kantanjs Bundle
* https://gitlab.com/kantan-fw/kantanjs/
* Copyright (c) 2017 Ezra Neuwirth
* Licensed under the MIT license (http://www.opensource.org/licenses/mit-license.php)
* @author : Ezra Neuwirth
* @version : 1.0.0
* 
* @preserve
* Kantan.Inputs.Autocomplete
* Creates Autocomplete Functions for input elements
* requires jquery
* for inputs Kantan.Inputs.Handler
 * for choose Kantan.Inputs.Choose
 * for typeahead https://github.com/bassjobsen/Bootstrap-3-Typeahead
 * for single|multiple types http://handlebarsjs.com/
*/

Kantan.Inputs.Autocomplete = function(currentApp,kantanInputs,kantanChoose)
{	
	this.app = currentApp;	
	this.objects = {		
		inputs : kantanInputs,
		choose : kantanChoose
	};
	
	this.callInputs = function()
	{
		return this.objects.inputs;
	};	
	this.callChoose = function()
	{
		return this.objects.choose;
	};	
};

Kantan.Inputs.Autocomplete.prototype = 
{	
	run : function(holder,inputHolder)
	{
		if(typeof(inputHolder.attr("data-loaded")) === "undefined")
	 	{
	 		var _self = this;	 	
	 		var itemsHolder = _self.callChoose().getItemsHolder(inputHolder);	
			inputHolder.attr("data-loaded","true");
			
			//for all the settings visist https://github.com/bassjobsen/Bootstrap-3-Typeahead
			
			var options = {		
				choose_type : "input",	//the available types input|single|multiple
				choose_value : false,		//the value to choose from the dataset
				convert_array : false,		//if you have indexes in your source data which are not numeric it will be converted via Kantan.Utilities.Data.objectToArray() function
				autoSelect: false,				//typeahead setting	
				items : 8,								//typeahead setting
			  	minLength : 1,		  			//typeahead setting
			  	afterSelect : function(selected_item)
			  	{			  		
			  		selected_item = _self.callChoose().setItem(
			  			selected_item,
			  			options
			  		);
			  		
			  		switch(options.choose_type)
			  		{			  			
			  			case "single":			  				
			  				_self.callChoose().setSingle(
			  					inputHolder,
			  					itemsHolder,
			  					selected_item
			  				);
			  				_self.changeSearch(inputHolder,"hide");				  				  
			  			break;
			  			
			  			case "multiple":			  				  			
			  				_self.callChoose().setMultiple(
				  				inputHolder,
				  				itemsHolder,
				  				selected_item,
				  				savedValues,
				  				"add"
				  			);
				  			_self.changeSearch(inputHolder,"clear");
			  			break;	
			  			
			  			case "input":
			  				if(typeof(inputHolder.data("kantanChange")) === "function")
							{
								inputHolder.data("kantanChange")({
									call : "setAutocomplete",
									kantanObject : _self,									
									inputHolder : inputHolder,
									data : selected_item,
									value : selected_item.input_id
								});
							}
			  				_self.callInputs().getInputField(inputHolder).val(selected_item.input_id);	
			  			break;		  			
			  		}			  		
			  	}
			};		
			options =_self.callInputs().getParameters(
				inputHolder,
				options
			);
			
			if(typeof(options.source) !== "undefined")
			{
				itemsHolder.attr("data-type",options.choose_type);
							
				switch(options.choose_type)
			  	{
			  		case "single":		  		
			  		case "multiple":
			  			_self.callInputs().setTemplate(inputHolder);			  						  		
			  		break;
			  	}
			  	
			  	var savedValues = null;			  	
			  	switch(options.choose_type)
			  	{	  		
			  		case "multiple":			  		
			  			savedValues = new Array();
			  			if(typeof(inputHolder.data("value")) === "object")
				  		{
				  			savedValues = inputHolder.data("value");		  			
				  		}	  				  	
			  		break;
			  	}
			  	
			  	var data_set = false;			  	
			  	if(typeof(options.source) === "object")
			  	{
			  		data_set = true;
			  	}
			  	
			  	if(typeof(options.source) === "string")
			  	{
			  		if(options.source == "api")
			  		{
			  			//TODO Working Example for an API call
			  			options.source = function(query,process) 
						{			 	
							//_self.app.callApi();							
						};
						_self.runAutoComplete(
							inputHolder,
							itemsHolder,
							options,
							savedValues,
							data_set
						);
			  		}else
			  		{			  		
			  			options.source_url = options.source;			 
			  			$.get(options.source_url, function(data)
					 	{
					 		if(typeof(data) === "string")
					 		{
					 			try
						 		{
						 			 options.source = $.parseJSON(data);					 			 
						 			 _self.runAutoComplete(
							  			inputHolder,
							  			itemsHolder,
							  			options,
							  			savedValues,
							  			data_set
							  		);					 			 		 		
						 		}catch(err) {
						 			console.log(err);
							   		console.log("Kantan.Inputs.Autocomplete : failed to load url " + options.source_url);
								}		
					 		}
					 		if(typeof(data) === "object")
					 		{
					 			options.source = data;					 			 
					 			 _self.runAutoComplete(
						  			inputHolder,
						  			itemsHolder,
						  			options,
						  			savedValues,
						  			data_set
						  		);		
					 		}					 			 	
						});			  					  		
			  		}	
			  	}else
			  	{
			  		_self.runAutoComplete(
			  			inputHolder,
			  			itemsHolder,
			  			options,
			  			savedValues,
			  			data_set
			  		);
			  	}		  
			}
	 	};		
	},
	
	runAutoComplete : function(inputHolder,itemsHolder,options,savedValues,data_set)
	{
		var _self = this;
		
	  	switch(options.choose_type)
	  	{
	  		case "single":
	  			if(typeof(typeof(inputHolder.data("value"))) !== "undefined")
	  			{
	  				var itemData = false;
	  				var saved_id = inputHolder.data("value");			  			
	  				if(data_set)
  					{
  						itemData = options.source[saved_id];
  					}else
  					{
  						if(typeof(inputHolder.data("setdata")) === "object")
						{							
							itemData = inputHolder.data("setdata");
						}
  					}  					
  					if(typeof(itemData) === "object")
	  				{
	  					var singleData = _self.callChoose().setItem(itemData,options);
	  					_self.callChoose().setSingle(
			  				inputHolder,
			  				itemsHolder,
			  				singleData
			  			);
			  			_self.changeSearch(inputHolder,"hide");
			  			if(inputHolder.find("[data-input=recycle]").length)
			  			{
			  				inputHolder.find("[data-input=recycle]").data("savedValue",singleData);
			  			}
	  				}	  					
	  			}			  				
	  		break;
	  		
	  		case "multiple":
	  			if(savedValues.length > 0)
	  			{
	  				for (saved_index in savedValues)
	  				{
	  					var saved_id = savedValues[saved_index];
	  					var itemData = false;			  					
	  					if(data_set)
	  					{
	  						itemData = options.source[saved_id];
	  					}else
	  					{
  							if(typeof(inputHolder.data("setdata")) === "object" && typeof(inputHolder.data("setdata")[saved_id]) === "object")
							{
								itemData = inputHolder.data("setdata")[saved_id];
							}
	  					}			  					
	  					if(typeof(itemData) === "object")
	  					{			  						
	  						_self.callChoose().setMultiple(
				  				inputHolder,
				  				itemsHolder,
				  				 _self.callChoose().setItem(
				  				 	itemData,
				  				 	options
				  				 ),
				  				 savedValues,
				  				 "show"	
				  			);				  						
	  					}				
	  				}			  				
	  				_self.changeSearch(inputHolder,"clear");			  				
	  			}			  			
	  		break;
	  	}
	  	
	  	if(options.convert_array && data_set)
		{			
			options.source = _self.app.callUtility().callData().objectToArray(options.source);
		}
	  	
	  	inputHolder.on("click","[data-multiple=item]",function(event)
		{
			var clickedItem = $(this);
			_self.callChoose().setMultipleSate(
				inputHolder,
				clickedItem,
				savedValues,
				"remove"
			);	 				
			if(itemsHolder.find("[data-multiple=item]").length == 0)
			{
				itemsHolder.removeClass(_self.app.css_classes.active);	 				
			}
		});
	  	
	  	inputHolder.on("click","[data-input=delete]",function(event)
		{	 			
			switch($(this).attr("data-type"))
			{
				case "single":
		  			_self.callChoose().removeSingle(inputHolder,itemsHolder);		
		  			_self.changeSearch(inputHolder,"show"); 							 		
					$(this).addClass(_self.app.css_classes.hidden);
		  		break;				  		
			}	 						 			
		});
		inputHolder.on("click","[data-input=recycle]",function(event)
		{
			var recycleButton = $(this);
			var savedValue = recycleButton.data("savedValue");			
			if(typeof(savedValue) === "object")
			{
				_self.callChoose().setSingle(
	  				inputHolder,
	  				itemsHolder,
	  				savedValue
	  			);
	  			_self.changeSearch(inputHolder,"hide");
	  			_self.callInputs().getInputField(inputHolder).val(savedValue.input_id);
			}			
		});
		 	
		inputHolder.find("input").typeahead(options);	
	},
	
	clear : function(inputHolder)
	{
		var _self = this;
		var itemsHolder = _self.callChoose().getItemsHolder(inputHolder);
		if(itemsHolder.length)
		{
			switch(itemsHolder.attr("data-type"))
			{
				case "single":
					if(inputHolder.find("[data-input=recycle]").length)
					{
						if(typeof(inputHolder.find("[data-input=recycle]").data("savedValue")) === "object")
						{
							inputHolder.find("[data-input=recycle]").data("savedValue",null);
						}
					}					
					_self.callChoose().removeSingle(inputHolder,itemsHolder);
					_self.changeSearch(inputHolder,"show");
				break;
				
				case "multiple":
					itemsHolder.html("");
					itemsHolder.removeClass(_self.app.css_classes.active);					
					_self.callChoose().clearMultiple(itemsHolder);
				break;
			}
		}	
	},
	
	changeSearch : function(inputHolder,action)
	{
		var _self = this;
		
		var searchHolder = inputHolder.find("[data-input=search]");
		if(searchHolder.length)
		{
			searchHolder.val("");
			switch(action)
			{
				case "hide":
					searchHolder.addClass(_self.app.css_classes.hidden);
				break;
				
				case "show":
					searchHolder.removeClass(_self.app.css_classes.hidden);
				break;
			}
		}
		var recycleHolder = inputHolder.find("[data-input=recycle]");
		if(recycleHolder.length)
		{
			recycleHolder.addClass(_self.app.css_classes.hidden);
			if(action == "show")
			{
				if(typeof(inputHolder.find("[data-input=recycle]").data("savedValue")) === "object")
				{
					recycleHolder.removeClass(_self.app.css_classes.hidden);
				}				
			}			
		}		
	}	
};