/*!
* kantanjs Bundle
* https://gitlab.com/kantan-fw/kantanjs/
* Copyright (c) 2017 Ezra Neuwirth
* Licensed under the MIT license (http://www.opensource.org/licenses/mit-license.php)
* @author : Ezra Neuwirth
* @version : 1.0.0
* 
* @preserve
* Kantan.Inputs.Editors
* Creates functions for editor elements
* requires jquery
*  tinymce  https://www.tinymce.com/
*/

Kantan.Inputs.Editors = function(currentApp)
{	
	this.app = currentApp;
};
Kantan.Inputs.Editors.prototype = 
{
	run : function(holder,inputHolder)
	{
		var _self = this;     	
		if(inputHolder.find(".mce-tinymce").length)
		{					
			return;
		}						
		var input_holder = inputHolder.find("textarea");		
		var editor_size = "small";
		if(typeof(inputHolder.attr("data-size")) === "string")
		{
			editor_size = inputHolder.attr("data-size");
		}
		
		var tinyMceOptions = {
			menubar : false,			
			statusbar : false,
			target: input_holder[0],			
			toolbar_items_size: 'small',			
			paste_word_valid_elements: "b,strong,i,em,p,a[href|target=_blank],br"		
		};
				
		switch(editor_size)
		{
			case "small":
				tinyMceOptions.toolbar = false;							
				tinyMceOptions.plugins = "autoresize paste";
				tinyMceOptions.autoresize_bottom_margin = 0;
				tinyMceOptions.valid_elements = 'br,p';
				tinyMceOptions.paste_word_valid_elements = 'br,p';				
			break;
			
			case "medium":
				tinyMceOptions.toolbar = 'removeformat code';						
				tinyMceOptions.plugins = 'paste code';
				tinyMceOptions.valid_elements = 'br,p,span';
				tinyMceOptions.paste_word_valid_elements = 'br,p';
				tinyMceOptions.autoresize_bottom_margin = 0;
			break;
			
			case "big":
				tinyMceOptions.toolbar = 'undo redo | bold italic blockquote | link unlink | visualblocks removeformat';						
				tinyMceOptions.plugins = 'autoresize link lists visualblocks paste';
				tinyMceOptions.autoresize_bottom_margin = 0;			
			break;
			
			case "large":
				tinyMceOptions.toolbar = 'undo redo | bold italic blockquote | bullist numlist | subscript superscript | link unlink | charmap | visualblocks code removeformat';
				tinyMceOptions.plugins = 'autoresize link lists visualblocks charmap paste code';
				tinyMceOptions.autoresize_bottom_margin = 0;
			break;
		}
		
		inputHolder.addClass("editor-" + editor_size);
		
		tinymce.init(
			_self.app.callInputs().getParameters(
				inputHolder,
				tinyMceOptions
			)
		);			
	}
};