/*!
* kantanjs Bundle
* https://gitlab.com/kantan-fw/kantanjs/
* Copyright (c) 2017 Ezra Neuwirth
* Licensed under the MIT license (http://www.opensource.org/licenses/mit-license.php)
* @author : Ezra Neuwirth
* @version : 1.0.0
* 
* @preserve
* Kantan.Inputs.Masks
* Creates typemasks for input elements
* requires jquery
* jquery.inputmask http://github.com/RobinHerbots/jquery.inputmask
*/

Kantan.Inputs.Masks = function(currentApp)
{	
	this.app = currentApp;
};
Kantan.Inputs.Masks.prototype = 
{
	run : function(holder,inputHolder)
	{
		var _self = this;
		
		var parameters = _self.app.callInputs().getParameters(
			inputHolder,
			{
				greedy: false
			}
		);					
		if(typeof(parameters) === "object")
		{						
			inputHolder.find("input").inputmask(parameters);
		}	
	}
};