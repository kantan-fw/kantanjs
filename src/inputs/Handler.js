/*!
* kantanjs Bundle
* https://gitlab.com/kantan-fw/kantanjs/
* Copyright (c) 2017 Ezra Neuwirth
* Licensed under the MIT license (http://www.opensource.org/licenses/mit-license.php)
* @author : Ezra Neuwirth
* @version : 1.0.0
* 
* @preserve
* Kantan.Inputs.Handler
* Handles functions/objcets for data-kantan-input elements
* requires jquery
* for state Kantan.Inputs.toggleState
 * for dropdown Kantan.Inputs.Dropdowns
 * for autocomplete Kantan.Inputs.Autocomplete
 * for choose Kantan.Inputs.Choose
 * for files Kantan.Inputs.Files
 * for datepicker Kantan.Inputs.Dates
 * for editor Kantan.Inputs.Editors
 * for mask Kantan.Inputs.Masks
 * for templates Kantan.Utilities.Templates
*/

Kantan.Inputs.Handler = function(currentApp)
{	
	this.app = currentApp;	
	this.objects = {
		toggleState : new Kantan.Inputs.toggleState(this.app),
		dropdown : new Kantan.Inputs.Dropdowns(this.app),		
		files : new Kantan.Inputs.Files(this.app),		
		masks : new Kantan.Inputs.Masks(this.app),
		editors : new Kantan.Inputs.Editors(this.app),
		dates : new Kantan.Inputs.Dates(this.app),
		selects : new Kantan.Inputs.Selects(this.app),
		languages : new Kantan.Inputs.Languages(this.app)				
	};	
	
	this.objects.choose = new Kantan.Inputs.Choose(this.app,this);	
	this.objects.autocomplete = new Kantan.Inputs.Autocomplete(
		this.app,
		this,
		this.objects.choose
	);
	
	this.callToggleState = function()
	{
		return this.objects.toggleState;
	};	
	this.callDropdown = function()
	{
		return this.objects.dropdown;
	};	
	this.callAutoComplete = function()
	{
		return this.objects.autocomplete;
	};	
	this.callChoose = function()
	{
		return this.objects.choose;
	};	
	this.callFiles = function()
	{
		return this.objects.files;
	};		
	this.callMasks = function()
	{
		return this.objects.masks;
	};	
	this.callEditors = function()
	{
		return this.objects.editors;
	};	
	this.callDates = function()
	{
		return this.objects.dates;
	};	
	this.callSelects = function()
	{
		return this.objects.selects;
	};	
	this.callLanguages = function()
	{
		return this.objects.languages;
	};	
};

Kantan.Inputs.Handler.prototype = 
{
	 getParameters : function(inputHolder,baseParameters)
    {
    	return this.app.callUtility().callData().getDataValues(
    		inputHolder,
    		"settings",
    		baseParameters
    	);
    },
	
	set : function(holder,data)
	{
		var _self = this;
		
		holder.find("[data-kantan-input]").each(function()
		{
			var inputHolder = $(this);			
			var input_name = _self.getInputName(inputHolder,true);
				
			if(typeof(data) === "object")
			{
				var data_to_search = data;							
				if(typeof(inputHolder.attr(_self.callLanguages().getAttribute())) !== "undefined")
				{
					if(typeof(data[_self.callLanguages().getKey()]) === "object"
						&& typeof(data[_self.callLanguages().getKey()][inputHolder.attr(_self.callLanguages().getAttribute())]) === "object")
					{
						data_to_search = data[_self.callLanguages().getKey()][inputHolder.attr(_self.callLanguages().getAttribute())];												
					}else
					{
						data_to_search = false;						
					}									
				}				
				if(typeof(data_to_search) === "object")
				{
					if(typeof(data_to_search[input_name]) !== "undefined")
					{					
						inputHolder.data("value",data_to_search[input_name]);								
					}				
					
					if(typeof(data_to_search['setdata']) === "object" && typeof(data_to_search['setdata'][input_name]) !== "undefined")
					{
						inputHolder.data("setdata",data_to_search['setdata'][input_name]);					
					}
				}				
			}
			
			switch(inputHolder.attr("data-kantan-input"))
			{
				case "state":
					_self.callToggleState().run(holder,inputHolder);
				break;
				
				case "date":
					_self.callDates().run(holder,inputHolder);					
				break;
				
				case "mask":
					_self.callMasks().run(holder,inputHolder);					
				break;
				
				case "editor":
					_self.callEditors().run(holder,inputHolder);				
				break;
				
				case "dropdown":					
					_self.callDropdown().run(holder,inputHolder);
				break;
				
				case "autocomplete":					
					_self.callAutoComplete().run(holder,inputHolder);					
				break;		
				
				case "choose":
					_self.callChoose().run(holder,inputHolder);
				break;
				
				case "file":
					_self.callFiles().run(inputHolder);
				break;
				
				case "select":
					_self.callSelects().run(holder,inputHolder);		
				break;				
			}			
		});
	},
	
	clear : function(holder)
	{
		var _self = this;
		
		holder.find("[data-kantan-input]").each(function()
		{
			var inputHolder = $(this);
			
			switch(inputHolder.attr("data-kantan-input"))
			{
				case "state":
					_self.callToggleState().clear(inputHolder);					
				break;
				
				case "date":
					inputHolder.prop("_flatpickr").clear();				
				break;
				
				case "dropdown":
					_self.callDropdown().clear(inputHolder);
				break;
				
				case "autocomplete":
					_self.callAutoComplete().clear(inputHolder);
				break;		
				
				case "choose":
					_self.callChoose().clear(inputHolder);
				break;
			}
		});
	},
	
	getHolderTitle : function(holder)
	{
		 var titleHolder = holder.find("[data-field=title]");
		 if(titleHolder.length)
		 {
		 	return titleHolder.html()
		 }
		 return false;
	},
	
	getInputHolder : function(holder,input_key)
	{
		 return holder.find("[data-kantan-field=" + input_key + "]");
	},
	
	getInputName : function(inputHolder,get_key)
	{
		var _self = this;
		
		if(typeof(inputHolder.attr("data-kantan-field")) === "undefined")
		{
			return false;
		}
		
		if(typeof(inputHolder.attr(_self.callLanguages().getAttribute())) !== "undefined")
		{
			var input_name = inputHolder.attr("data-kantan-field").replace("-" + inputHolder.attr(_self.callLanguages().getAttribute()),"");			
			if(get_key)
			{
				return input_name;
			}						
			input_name = _self.callLanguages().getKey() + "[" + inputHolder.attr(_self.callLanguages().getAttribute()) + "]["+ input_name +"]";					
			return input_name;
		}		
		return inputHolder.attr("data-kantan-field");
	},
	
	getInputField : function(inputHolder)
	{		
		return inputHolder.find("[data-input-value]")
	},
	
	getInputValue : function(inputHolder,default_value)
	{
		var inputValue = this.getInputField(inputHolder);		
		if(inputValue.length && inputValue.val().length > 0)
 		{
 			return inputValue.val();
		}
		if(typeof(default_value) !== "undefined")
		{
			return default_value;
		}		
		return null;
	},
	
	setTemplate : function(inputHolder)
	{
		return this.app.callUtility().callTemplates().compile(
			inputHolder,
			"[data-input=template]"
		);		
	},
	
	loadTemplate : function(inputHolder,data)
	{
		return this.app.callUtility().callTemplates().load(
			inputHolder,
			data
		);		
	}	
};