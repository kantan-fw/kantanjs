/*!
* kantanjs Bundle
* https://gitlab.com/kantan-fw/kantanjs/
* Copyright (c) 2017 Ezra Neuwirth
* Licensed under the MIT license (http://www.opensource.org/licenses/mit-license.php)
* @author : Ezra Neuwirth
* @version : 1.0.0
* 
* @preserve
* Kantan.Inputs.toggleState
* Creates  toogle Functions for elements
* requires jquery
*/

Kantan.Inputs.toggleState = function(currentApp)
{	
	this.app = currentApp;
};

Kantan.Inputs.toggleState.prototype = 
{
	getParameters : function(inputHolder)
	 {	 	
	 	var _self = this;	 	
	 	
 		var parameters =_self.app.callInputs().getParameters(
 			inputHolder,
 			{	 				
 				state_default : "btn-light", //the default css class
 				state_active : "btn-success", //the default active class
 				toggle : false	//if true only one button will be visible, if no value is present the first button will be shown
 			}
 		); 		
 		
 		parameters.value_to_set = false; 		 	
 		if(typeof(inputHolder.data("value")) !== "undefined")
 		{
 			parameters.value_to_set = inputHolder.data("value"); 			
 		}
 		
 		return parameters;
	 },
	 
	run : function(holder,inputHolder)
	 {
	 	var _self = this;		
	 	
	 	if(typeof(inputHolder.attr("data-loaded")) === "undefined")
	 	{
	 		inputHolder.attr("data-loaded","true");	 		 		
	 		var parameters =_self.getParameters(inputHolder);
	 		
	 		inputHolder.on("click","[data-state=button]",function(event)	 	
	 		{
				event.stopPropagation();	
	 		 	event.preventDefault();
	 		 	
				var stateButton = $(this);
				
				if(parameters.toggle)
				{
					if(stateButton.next().length)
					{
						_self.toogleStateButton(
							inputHolder,
							stateButton.next(),
							parameters
						);			
					}else
					{
						_self.toogleStateButton(
							inputHolder,
							inputHolder.find("[data-state=button]:first"),
							parameters
						);							
					}
				}else
				{
					_self.setStateButtons(
						inputHolder,
						parameters,
						stateButton
					);
				}			
	 		 });
	 		 
	 		 _self.setValue(inputHolder,parameters);	 		
	 	}	 	
	 },
	 
	 clear : function(inputHolder)
	 {
	 	var _self = this;	 	
	 	
	 	_self.setValue(
	 		inputHolder,
	 		_self.getParameters(inputHolder),
	 		true
	 	);	 	
	 },
	 
	 setValue : function(inputHolder,parameters,clearValue)
	 {
	 	var _self = this;	 	
	 	
	 	 if(parameters.value_to_set)
 		 {
 		 	var activeInput = inputHolder.find("input[value=" + parameters.value_to_set + "]");
 		 	if(activeInput.length)
	 		 {
	 		 	if(parameters.toggle)
				{					
					_self.toogleStateButton(
						inputHolder,
						activeInput.parent(),
						parameters,
						true
					);					
				}else
				{
					activeInput.parent().trigger("click");
				}	
	 		 } 	 		 
 		 }else
 		 {
 		 	if(parameters.toggle)
			{			 		 	
	 		 	_self.toogleStateButton(
	 		 		inputHolder,
	 		 		inputHolder.find("[data-state=button]:first"),
	 		 		parameters,
	 		 		true
	 		 	);
	 		}else
	 		{
	 			if(clearValue)
	 			{
	 				_self.setStateButtons(
	 					inputHolder,
	 					parameters
	 				);	
	 				
	 				inputHolder.data("kantanChange")({
						call : "removeState",				
						kantanObject : _self,
						inputHolder : inputHolder
					});
	 			}
	 		}
 		 }
	 },
	 
	 setActive : function(inputHolder,activeButton,button_parameters)
	 {
	 	var _self = this;
	 	
	 	activeButton.addClass(button_parameters.state_active);
		activeButton.find("input").attr('checked', 'checked');
		
	 	if(typeof(inputHolder.data("kantanChange")) === "function")
		{			
			inputHolder.data("kantanChange")({
				call : "changeState",				
				kantanObject : _self,
				inputHolder : inputHolder,
				value : activeButton.find("input").val()
			});
		}
	 },
	 
	 toogleStateButton : function(inputHolder,activeButton,parameters,init)
	 {
	 	var _self = this;	 	
	 	var button_parameters =_self.app.callInputs().getParameters(
 			activeButton,
 			parameters
 		);
 		
	 	inputHolder.find("[data-state=button]").addClass(_self.app.css_classes.hidden);
		inputHolder.find("[data-state=button]").find("input").removeAttr('checked');	
	 	
		activeButton.removeClass(_self.app.css_classes.hidden);
		activeButton.removeClass(button_parameters.state_default);
		
		_self.setActive(
			inputHolder,
			activeButton,
			button_parameters
		);
		
		if(init)
		{
			inputHolder.find("[data-state=holder]").removeClass("init");
		}		
	 },
	 
	setStateButtons : function(inputHolder,parameters,activeButton)
	 {
	 	var _self = this;
	 	
	 	inputHolder.find("[data-state=button]").each(function()
		{
			var stateButton = $(this);			
			var button_parameters =_self.app.callInputs().getParameters(
	 			stateButton,
	 			parameters
	 		);	 		
	 		stateButton.removeClass(button_parameters.state_default);
	 		stateButton.removeClass(button_parameters.state_active);
	 		
	 		var clear_state = true;	 		
	 		if(typeof(activeButton) === "object" && activeButton.find("input").val() == stateButton.find("input").val())
	 		{
	 			_self.setActive(
					inputHolder,
					stateButton,
					button_parameters
				);
	 				 			
	 			clear_state = false;	 			
	 		}
	 		if(clear_state)
	 		{
	 			stateButton.find("input").removeAttr('checked');
	 			stateButton.addClass(button_parameters.state_default);	 			
	 		}		
		});	
	 },
};