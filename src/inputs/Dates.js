/*!
* kantanjs Bundle
* https://gitlab.com/kantan-fw/kantanjs/
* Copyright (c) 2017 Ezra Neuwirth
* Licensed under the MIT license (http://www.opensource.org/licenses/mit-license.php)
* @author : Ezra Neuwirth
* @version : 1.0.0
* 
* @preserve
* Kantan.Inputs.Dates
* creates functions for choosing a date/time
* requires jquery
* flatpickr https://github.com/chmln/flatpickr
*/

Kantan.Inputs.Dates = function(currentApp)
{	
	this.app = currentApp;
};
Kantan.Inputs.Dates.prototype = 
{
	run : function(holder,inputHolder)
	{
		var _self = this;		
		
		var calendarOptions = {
			enableTime: false,		 	
			wrap: true,
			allowInput : false,
			disableMobile : true			
		};		
		
		var date_type = "date";
		if(typeof(inputHolder.attr("data-pick")) === "string")
		{
			date_type = inputHolder.attr("data-pick");
		}		
		switch(date_type)
    	{    		
    		case "date-time":    			
    			calendarOptions.enableTime = true;	
    			calendarOptions.time_24hr = true;
    		break;
    		
    		case "time":    					
    			calendarOptions.enableTime = true;	
				calendarOptions.noCalendar = true;
				calendarOptions.enableSeconds = false;
				calendarOptions.time_24hr = true;
				calendarOptions.disableMobile = false;
    		break;
    	}		
		calendarOptions = _self.app.callInputs().getParameters(
			inputHolder,
			calendarOptions
		);
		
		calendarOptions.onChange = function(selectedDates, dateStr, instance)
		{			
			var callBackCall = "changeDate";
			if(selectedDates.length > 0)
			{
				inputHolder.find("[data-clear]").removeClass(_self.app.css_classes.hidden);
			}else
			{
				inputHolder.find("[data-clear]").addClass(_self.app.css_classes.hidden);
				callBackCall = "removeDate";
			}			
			if(typeof(inputHolder.data("kantanChange")) === "function")
			{
				inputHolder.data("kantanChange")({
					call : callBackCall,	
					kantanObject : _self,
					inputHolder : inputHolder,
					value : selectedDates
				});
			}
		};
				
		inputHolder.flatpickr(calendarOptions);	
	}
};