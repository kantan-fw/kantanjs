/*!
* kantanjs Bundle
* https://gitlab.com/kantan-fw/kantanjs/
* Copyright (c) 2017 Ezra Neuwirth
* Licensed under the MIT license (http://www.opensource.org/licenses/mit-license.php)
* @author : Ezra Neuwirth
* @version : 1.0.0
* 
* @preserve
* Kantan.Inputs.Files
* Creates functions for file uploads
* requires jquery
* dropzone http://www.dropzonejs.com/ Modfied in Code Line 606 for workaround of hiddenFileInput
*/

Kantan.Inputs.Files = function(currentApp)
{	
	this.app = currentApp;
	
	this.css_classes = {		
		deleting : "deleting"
	};
};

Kantan.Inputs.Files.prototype = 
{
	getParameters : function(inputHolder,fileHolder,input_name)
	 {	 	
	 	var _self = this;	 	
	 	
 		var parameters =_self.app.callInputs().getParameters(
 			inputHolder,
 			{	 		 					
 				url : "no-url",
 				type : "image",
 				acceptedFiles : null,
 				parallelUploads : 1,
 				paramName : null,
 				resizeWidth : 1900, 				
 				thumbnailMethod : "contain",
 				maxFiles : 1,
 				autoProcessQueue: false,
 				clickable : false,
 				dictMaxFilesExceeded : "You can not upload any more files." ,
 				dictResetNotPossible : "You exeeded your max. files, remove another file first." ,
 				dictInvalidFileType : "You can't upload files of this type."		
 			}
 		);
 		 
 		if(parameters.url == "no-url")
 		{
 			parameters.autoProcessQueue = false; 			
 			parameters.accept = function(file, done)
			{								
				var image_counter = this.files.length;				
				var file_input_name = input_name;
				
				reader = new FileReader();		
				reader.onload = function(evt) 
        		{		        			
        			if(parameters.maxFiles > 1)
        			{
        				file_input_name += "[" + image_counter + "]";
        			}
					
        			if(typeof(JSON.stringify) === "function")
        			{        				
        				var fileSettings = JSON.stringify({
	        				type : file.type,
	        				name : file.name,
	        				size : file.size,
	        				base64 : evt.target.result
	        			});	        			
	        			$(file.previewElement).append(
	        				"<input type='hidden' name='" + file_input_name + "' value='" + fileSettings + "' />"
	        			);
        			} else
        			{
        				$(file.previewElement).append(
	        				"<input type='hidden' name='" + file_input_name + "' value='" + evt.target.result + "' />"
	        			);
        			}        			
        			done();        				
        		};
				reader.readAsDataURL(file);				
			}; 			
 		} 		
 		switch(parameters.type)
 		{
 			case "image":
 				if(parameters.acceptedFiles == null)
 				{ 					
 					parameters.acceptedFiles = "image/jpg,image/jpeg,image/pjpeg,image/png,image/x-png"; 					
 				} 				
 			break;
 		}
 		
 		//Works only because the used dropzone.js File was modified so it can accept objects (Code Line 606 dropzone.js)
		parameters.hiddenInputContainer = inputHolder; 		
 		parameters.previewTemplate = inputHolder.find("[data-file=template]").html();
 		parameters.clickable = inputHolder.find("[data-file=choose]")[0];
 		 
 		parameters.init = function() 
		{
			this.on("addedfile", function(file)
			{				
				if(parameters.maxFiles == 1)
 				{
 					if (this.files[1] != null)
	  				{
	    				this.removeFile(this.files[0]);
	  				}
	  				inputHolder.find("[data-existing=item]").addClass(_self.app.css_classes.hidden);
 				}else
 				{ 				
 					var file_count = _self.getFileCount(fileHolder,this.files.length);
 					if(file_count > parameters.maxFiles)
 					{
 						_self.app.callUtility().callForms().setErrorToInput(
 							inputHolder,
 							parameters.dictMaxFilesExceeded
 						); 						
 						this.removeFile(this.files[this.files.length-1]);
 					}				
 				}
			});
			this.on("removedfile", function(file) 
			{	
				if(parameters.maxFiles == 1)
 				{
 					inputHolder.find("[data-existing=item]").removeClass(_self.app.css_classes.hidden);
 				}else
 				{
 					var file_count = _self.getFileCount(fileHolder,this.files.length); 					
 					if(file_count < parameters.maxFiles)
 					{
 						_self.app.callUtility().callForms().removeErrorFromInput(inputHolder); 						  	
 					}
 				}
			});							
		};
		
 		return parameters;
	 },
	 
	 getFileCount : function(inputHolder,file_length)
	 {
	 	return inputHolder.find("[data-existing=item]").length + file_length;
	 },
	
	run : function(inputHolder)
	{	
		var _self = this;		
	 	
	 	if(typeof(inputHolder.attr("data-loaded")) === "undefined")
	 	{
	 		inputHolder.attr("data-loaded","true");	 		 		
	 		
	 		var fileHolder = inputHolder.find("[data-file=holder]");	 		
	 		var input_name = _self.app.callInputs().getInputName(inputHolder);
	 		
	 		var parameters =_self.getParameters(
	 			inputHolder,
	 			fileHolder,
	 			input_name
	 		);	 		
	 		
	 		fileHolder.dropzone(parameters);	
	 				 		
	 		if(typeof(inputHolder.data("value")) === "object")
	 		{	 			
	 			_self.app.callInputs().setTemplate(inputHolder);
	 			
	 			if(parameters.maxFiles > 1)
				{					
					for(image_index in inputHolder.data("value"))
					{											
						var existingFile = inputHolder.data("value")[image_index];
						if(typeof(existingFile.file_index) === "undefined")
						{
							existingFile.file_index = image_index;	
						}												
						fileHolder.append(
							_self.app.callInputs().loadTemplate(inputHolder,existingFile)
						);			
					}
				}else
				{
					fileHolder.append(
						_self.app.callInputs().loadTemplate(inputHolder,inputHolder.data("value"))
					);
				}
				inputHolder.on("click","[data-file=delete]",function(event)	 	
		 		{	 			
		 			var currentItem = $(this).parents("[data-file=item]");		 			
		 			var item_input_name = input_name;
		 			
		 			_self.app.callUtility().callForms().removeErrorFromInput(
						inputHolder
					);
		 						
		 			if(currentItem.hasClass(_self.css_classes.deleting))
		 			{
		 				var reset_state = true;
		 				 if(parameters.maxFiles > 1)
	        			{	  	        			
	        				var file_count = _self.getFileCount(
	        					inputHolder,
	        					inputHolder.find("[data-new=item]").length
	        				);	        						        				
	        				if(file_count > parameters.maxFiles)
	        				{
	        					reset_state = false;
	        					_self.app.callUtility().callForms().setErrorToInput(
		 							inputHolder,
		 							parameters.dictResetNotPossible
		 						);
		 					}
	        			}	 				
		 				if(reset_state)
		 				{
		 					currentItem.removeClass(_self.css_classes.deleting);
			 				currentItem.find("input[type=hidden]").remove();
			 				if(parameters.maxFiles > 1)
	        				{
			 					currentItem.attr("data-existing","item");
			 				}		
		 				}		 								 		
		 			}else
		 			{
		 				currentItem.addClass(_self.css_classes.deleting);		 						 					
		 				if(parameters.maxFiles > 1)
        				{
        					item_input_name += "["+ currentItem.attr("data-index") + "]";
        				}		 				
		 				currentItem.append(
	        				"<input type='hidden' name='delete-" + item_input_name + "' value='delete' />"
	        			);
	        			if(parameters.maxFiles > 1)
        				{				        			
	        				currentItem.attr("data-existing","item-deleting");
	        			}	        				 					 			
		 			}
		 		});				
	 		}	 			
	 	}	
	},
	
	removeFiles : function(inputHolder)
	{
		//inputHolder
		//_self.objdrop.removeAllFiles(true);		
	}
};