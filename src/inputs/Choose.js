/*!
* kantanjs Bundle
* https://gitlab.com/kantan-fw/kantanjs/
* Copyright (c) 2017 Ezra Neuwirth
* Licensed under the MIT license (http://www.opensource.org/licenses/mit-license.php)
* @author : Ezra Neuwirth
* @version : 1.0.0
* 
* @preserve
* Kantan.Inputs.Choose
* creates functions for choosing an single/multiple item as an input
* requires jquery
* for inputs Kantan.Inputs.Handler
*/

Kantan.Inputs.Choose = function(currentApp,kantanInputs)
{	
	this.app = currentApp;
	this.objects = {		
		inputs : kantanInputs		
	};	
	this.callInputs = function()
	{
		return this.objects.inputs;
	};	
	
	this.css_classes = {
		add : "added",		
		remove : "remove",		
		saved : "saved",
		deleting : "deleting"
	};
};

Kantan.Inputs.Choose.prototype = 
{	
	getItemsHolder : function(inputHolder)
	{
		return inputHolder.find("[data-kantan-items=holder]");
	},
	
	getItemHolderFromData : function(itemsHolder,data)
	{
		return itemsHolder.find("[data-item-id='" + data.input_id + "']");
	},
	
	setItem : function(data,options)
	{
		data.input_id = data.name; 		
  		var change_value = false;	  		
  		if(typeof(options.choose_value) !== "undefined" && typeof(data[options.choose_value]) !== "undefined")
  		{	
  			data.input_id = data[options.choose_value];	  		
  			data.change_value = true;		  			
  		}  		
		return data;
	},
	
	run : function(holder,inputHolder)
	{
		if(typeof(inputHolder.attr("data-loaded")) === "undefined")
	 	{
	 		var _self = this;
	 		var itemsHolder = _self.getItemsHolder(inputHolder);
	 		inputHolder.attr("data-loaded","true");	 		
	 		
	 		var options = {						
				choose_value : false				
			};	
			options =_self.callInputs().getParameters(
				inputHolder,
				options
			);
			
			if(typeof(options.source) === "object")
			{
				_self.callInputs().setTemplate(inputHolder);
	  			
	  			var savedValues = new Array();
	  			if(typeof(inputHolder.data("value")) === "object")
		  		{
		  			savedValues = inputHolder.data("value");		  			
		  		}
		  		
		  		for (value_index in options.source)
	  			{
	  				 var itemData = _self.setItem(
	  				 	options.source[value_index],
	  				 	options
	  				 );	  				
	  				_self.setMultiple(
		  				inputHolder,
		  				itemsHolder,
						itemData,
						savedValues,
						"show"						
		  			);
	  			}
	  			
	  			inputHolder.on("click","[data-multiple=item]",function(event)
	 			{
	 				var clickedItem = $(this);
	 				_self.setMultipleSate(
	 					inputHolder,
	 					clickedItem,
	 					savedValues,
	 					"change"
	 				);
	 			});
			}
	 	}
	},
	
	clear : function(inputHolder)
	{
		var _self = this;
		var itemsHolder = _self.getItemsHolder(inputHolder);
		if(itemsHolder.length)
		{								
			itemsHolder.find("[data-multiple=item]").removeClass(_self.css_classes.add);
			itemsHolder.find("[data-multiple=item]").find("input[type=hidden]").remove();			
			_self.clearMultiple(itemsHolder);
		}
	},
	
	clearMultiple : function(inputHolder)
	{
		if(typeof(inputHolder.data("kantanChange")) === "function")
		{
			inputHolder.data("kantanChange")({
				call : "clearMultiple",				
				kantanObject : this,
				inputHolder : inputHolder					
			});
		}			
	},
	
	setMultipleSate : function(inputHolder,itemHolder,savedValues,action)
	{
		var _self = this;
		var indexValue = savedValues.indexOf(itemHolder.attr("data-item-id"));		
		var input_action = false;		
		var input_element = false;
		var run_callback = true;
		
		if(indexValue == -1)
		{
			var css_classes = {
				"add" : _self.css_classes.add,
				"remove" : _self.css_classes.remove
			};
		}else
		{
			var css_classes = {
				"add" : _self.css_classes.saved,
				"remove" : _self.css_classes.deleting
			};
		}
		
		if(action == "remove")
		{
			if(indexValue == -1)
			{
				itemHolder.remove();
			}else
			{
				action = "change";
			}
		}
		
		switch(action)
		{
			case "add":
				itemHolder.addClass(css_classes.add);				
				input_element = "save";				
			break;
			
			case "show":
				if(indexValue == -1)
				{
					itemHolder.addClass(css_classes.remove);
					run_callback = false;
				}else
				{
					itemHolder.addClass(css_classes.add);					
				}	
			break;
			
			case "change":
				input_element = "remove";
				
				if(itemHolder.hasClass(css_classes.add))
				{
					itemHolder.removeClass(css_classes.add);
					itemHolder.addClass(css_classes.remove);							
					if(indexValue >= 0)
					{
						input_element = "delete";						
					}																
				}else
				{
					itemHolder.addClass(css_classes.add);
					itemHolder.removeClass(css_classes.remove);
					if(indexValue == -1)
					{
						input_element = "save";						
					}					 						
				}				
			break;			
		}
		
		if(run_callback && typeof(inputHolder.data("kantanChange")) === "function")
		{
			var callBackCall = "setMultiple";			
			switch(action)
			{
				case "remove":
					callBackCall = "removeMultiple";
				break;
				
				case "change":
					if(!itemHolder.hasClass(css_classes.add))
					{
						callBackCall = "removeMultiple";
					}
				break;
			}
			
			inputHolder.data("kantanChange")({
				call : callBackCall,				
				kantanObject : _self,
				inputHolder : inputHolder,						
				value : itemHolder.attr("data-item-id")
			});
		}
				
		switch(input_element)
		{
			case "remove":
				itemHolder.find("input[type=hidden]").remove();
			break;
			
			case "save":			
			case "delete":
				var input_name = _self.callInputs().getInputName(inputHolder);				
				if(input_element == "delete")
				{
					input_name = "delete-" + input_name;
				}				
				itemHolder.append(
					"<input type='hidden' name='" + input_name + "[" + itemHolder.attr("data-item-id") + "]' value='" + itemHolder.attr("data-item-id") + "' />"
				);				
			break;
		}
	},
	
	setMultiple : function(inputHolder,itemsHolder,data,savedValues,action)
	{
		var _self = this;	
		if(!_self.getItemHolderFromData(itemsHolder,data).length)
		{			
			itemsHolder.append(
				_self.callInputs().loadTemplate(inputHolder,data)
			);		
			itemsHolder.addClass(_self.app.css_classes.active);			
			
			var itemHolder = _self.getItemHolderFromData(itemsHolder,data);		  			
  			if(itemHolder.length)
  			{
  				_self.setMultipleSate(
  					inputHolder,
 					itemHolder,
 					savedValues,
 					action 					
 				);	 
  			}			
		}		
	},
	
	setSingle : function(inputHolder,itemsHolder,data)
	{
		var _self = this;		
		_self.callInputs().getInputField(inputHolder).val(data.input_id);
		
		if(typeof(inputHolder.data("kantanChange")) === "function")
		{
			inputHolder.data("kantanChange")({
				call : "setSingle",
				kantanObject : _self,
				inputHolder : inputHolder,
				data : data,
				value : data.input_id
			});
		}
		
		itemsHolder.html(
			_self.callInputs().loadTemplate(inputHolder,data)
		);
		
		itemsHolder.addClass(_self.app.css_classes.active);
		inputHolder.find("[data-input=delete]").removeClass(_self.app.css_classes.hidden);			
	},
	
	removeSingle : function(inputHolder,itemsHolder)
	{
		var _self = this;		
		var inputField = _self.callInputs().getInputField(inputHolder);
		
		if(typeof(inputHolder.data("kantanChange")) === "function")
		{
			inputHolder.data("kantanChange")({
				call : "removeSingle",
				kantanObject : _self,
				inputHolder : inputHolder,
				value : inputField.val()			
			});
		}
		
		itemsHolder.html("");
		inputField.val("");
		itemsHolder.removeClass(_self.app.css_classes.active);				
	}
};