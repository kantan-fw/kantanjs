/*!
* kantanjs Bundle
* https://gitlab.com/kantan-fw/kantanjs/
* Copyright (c) 2017 Ezra Neuwirth
* Licensed under the MIT license (http://www.opensource.org/licenses/mit-license.php)
* @author : Ezra Neuwirth
* @version : 1.0.0
* 
* @preserve
* Kantan.Inputs.Dropdowns
* an extension to the bootstrap dropdown
* requires jquery/bootstrap
*/

Kantan.Inputs.Dropdowns = function(currentApp)
{	
	this.app = currentApp;
};

Kantan.Inputs.Dropdowns.prototype = 
{
	run : function(holder,inputHolder)
	{
		var _self = this;
		
		if(typeof(inputHolder.attr("data-loaded")) === "undefined")
	 	{
	 		inputHolder.attr("data-loaded","true");	 	
	 		var parameters =_self.app.callInputs().getParameters(
	 			inputHolder,
	 			{	 				
	 				remove_value : false,	 											//if true you can remove the selected value		
	 				keep_open : false,														//if true the dropdown stays open
	 				state_active : _self.app.css_classes.active,		// The class to indicate that the item is active if the dropdown is open
	 				default_message : "Choose your option",		//the default message if no item is selected
	 				language_switch : false												//if true this dropdown will be uses as an language switch via Kantan.Inputs.Languages
	 			}
	 		);
	 		
	 		var inputField = _self.app.callInputs().getInputField(inputHolder);
	 		var value_to_set = _self.app.callInputs().getInputValue(inputHolder,value_to_set);
	 		if(!value_to_set && typeof(inputHolder.data("value")) !== "undefined")
	 		{
	 			value_to_set = inputHolder.data("value"); 			
	 		}	 		
	 		
	 		inputHolder.on("click","[data-dropdown=delete]",function(event)
	 		{
	 			inputHolder.find("[data-dropdown=item]").removeClass(parameters.state_active);
	 			
	 			if(typeof(inputHolder.data("kantanChange")) === "function")
				{
					inputHolder.data("kantanChange")({
						call : "removeDropdown",				
						kantanObject : _self,
						inputHolder : inputHolder,
						value : inputField.val()			
					});
				}
	 			
 				inputField.val("");	 				
 				inputHolder.find("[data-toggle=dropdown]").html(parameters.default_message);
 				$(this).addClass(_self.app.css_classes.hidden);
			 				
	 		});
	 		
	 		inputHolder.on("click","[data-dropdown=item]",function(event)	 	
	 		{
	 			if(parameters.keep_open)
	 			{
	 				event.stopPropagation();
	 				event.preventDefault();
	 			}
	 			
	 			var dropdownItem = $(this);	 			
	 			inputHolder.find("[data-dropdown=item]").removeClass(parameters.state_active);	 			
	 			
	 			_self.setTitle(inputHolder,dropdownItem);
	 			
	 			if(parameters.language_switch)
	 			{	 				
	 				_self.app.callInputs().callLanguages().set(
		 				holder,
		 				dropdownItem.attr("data-value")
		 			);
	 			}
	 			
	 			if(typeof(inputHolder.data("kantanChange")) === "function")
				{
					inputHolder.data("kantanChange")({
						call : "setDropdown",				
						kantanObject : _self,
						inputHolder : inputHolder,
						value : dropdownItem.attr("data-value")			
					});
				}
	 			
				inputField.val(dropdownItem.attr("data-value"));				
				dropdownItem.addClass(parameters.state_active);
				if(parameters.remove_value)
				{
					inputHolder.find("[data-dropdown=delete]").removeClass(_self.app.css_classes.hidden);	
				}					 			
	 		});
	 		
	 		if(value_to_set)
	 		{
	 			var activeItem = inputHolder.find("[data-value='" + value_to_set + "']");
	 			if(activeItem.length)
	 			{
	 				activeItem.trigger("click");
	 			}
	 		}
	 	}
	},
	
	setTitle : function(inputHolder,dropdownItem)
	 {
	 	inputHolder.find("[data-toggle=dropdown]").html(
			dropdownItem.find("[data-dropdown=title]").html()
		);
	 },
	 
	 clear : function(inputHolder)
	 {	 		 
	 	inputHolder.find("[data-dropdown=delete]").trigger("click");
	 }
};