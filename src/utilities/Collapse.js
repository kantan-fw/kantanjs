/*!
* kantanjs Bundle
* https://gitlab.com/kantan-fw/kantanjs/
* Copyright (c) 2017 Ezra Neuwirth
* Licensed under the MIT license (http://www.opensource.org/licenses/mit-license.php)
* @author : Ezra Neuwirth
* @version : 1.0.0
* 
* @preserve
* Kantan.Utilities.Collapse
* A small Utility Class to handle Bootstrap Collapse
* requires jquery/bootstrap
* 
*/

Kantan.Utilities.Collapse = function(currentApp)
{	
	this.app = currentApp;
};

Kantan.Utilities.Collapse.prototype = 
{		
	init : function()
	{
		$('body').on('click.collapse-next.data-api', '[data-toggle=collapse-next]', function (e) 
		{
			var collapseButton = $(this);			
			var collapseTarget = $(this).next();
			
			collapseButton.toggleClass("collapsed");
  			collapseTarget.collapse('toggle');	
		});
	}
};