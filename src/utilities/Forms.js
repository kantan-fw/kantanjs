/*!
* kantanjs Bundle
* https://gitlab.com/kantan-fw/kantanjs/
* Copyright (c) 2017 Ezra Neuwirth
* Licensed under the MIT license (http://www.opensource.org/licenses/mit-license.php)
* @author : Ezra Neuwirth
* @version : 1.0.0
* 
* @preserve
* Kantan.Utilities.Forms
* A small Utility Class to handle Forms
* requires jquery
* 
*/

Kantan.Utilities.Forms = function(currentApp)
{	
	this.app = currentApp;
};

Kantan.Utilities.Forms.prototype = 
{
	css_classes: {		
		validated : "was-validated",
		error : "is-invalid",
		valid : "is-vald"
	},
	
	init : function(holder,onComplete)
	{
		var _self = this;
				
		holder.on("submit","[data-kantan-form]",function(event)
		{
			var formHolder = $(this);
			event.preventDefault();
        	event.stopPropagation();        	        
			_self.run(
				formHolder,
				event,
				onComplete
			);							
		});
		
		holder.on("keypress","[data-kantan-form-function=noenter]",function(event)
		{
			if (event.keyCode == 13) 
			{
				event.preventDefault();
                return false;
            }			
		});			
	},
	
	run : function(formHolder,form_event,onComplete)
	{
		var _self = this;		
		var kantanHolder = _self.app.callHolders().getHolder(formHolder);		
		
		_self.app.callHolders().scrollTo(kantanHolder);		
		_self.changeValidation(formHolder);		
		_self.setLoading(kantanHolder,formHolder);
		_self.removeErrors(formHolder);
		
		if(formHolder[0].checkValidity())
		{
			if(typeof(onComplete) === "function")
			{
				onComplete(kantanHolder,formHolder,form_event);
			}	
		}else
		{
			_self.setErrors(kantanHolder,formHolder);
		}
	},
	
	changeValidation : function(formHolder,remove)
	{
		var _self = this;		
		
		if(remove)
		{
			formHolder.removeClass(_self.css_classes.validated);	
		}else
		{
			formHolder.addClass(_self.css_classes.validated);	
		}		
	},

	setLoading : function(kantanHolder,formHolder)
	{
		var _self = this;		
		
		_self.app.callUtility().callLoading().loading(kantanHolder);		
		formHolder.find("[data-form-save]").addClass(_self.app.css_classes.loading);			
		formHolder.find("[data-form-state]").addClass(_self.app.css_classes.hidden);
		
		_self.removeErrors(formHolder);
	},
	
	setComplete : function(kantanHolder,formHolder)
	{
		var _self = this;		
		
		_self.app.callUtility().callLoading().complete(kantanHolder);			
		formHolder.find("[data-form-save]").removeClass(_self.app.css_classes.loading);				
		
		if(formHolder.find("[data-form-state=saved]").length)
		{
			var stateSavedHolder = formHolder.find("[data-form-state=saved]");
			
			stateSavedHolder.removeClass(_self.app.css_classes.hidden);			
			if(stateSavedHolder.attr("data-timeout") !== "undefined")
			{
				setTimeout(function()
				{
					stateSavedHolder.addClass(_self.app.css_classes.hidden);	 
				}, stateSavedHolder.attr("data-timeout"));	
			}			
		}			
	},
	
	removeErrors : function(formHolder)
	{
		var _self = this;		
		formHolder.find("[data-kantan-input]").removeClass(_self.css_classes.error);		
		formHolder.find("[data-kantan=feedback]").each(function()
		{				
			_self.resetFeedback(
				$(this)
			);
		});
	},
	
	resetFeedback : function(feedbackHolder)
	{
		if(feedbackHolder.attr("data-default") !== "undefined")
		{
			feedbackHolder.html(feedbackHolder.attr("data-default"));	
		}else
		{
			feedbackHolder.html("");
		}	
	},
	
	setErrors : function(kantanHolder,formHolder,formErrors)
	{
		var _self = this;
		
		_self.app.callUtility().callLoading().complete(kantanHolder);		
		formHolder.find("[data-form-save]").removeClass(_self.app.css_classes.loading);		
		formHolder.find("[data-form-state=error]").removeClass(_self.app.css_classes.hidden);
		
		var errorMenue = formHolder.find("[data-error-menue]");
		if(errorMenue.length)
		{			
			errorMenue.find("[data-error-titles]").html("");			
		}
		
		if(typeof(formErrors) !== "undefined")
		{			
			formHolder.find("[data-error-count]").html(Object.keys(formErrors).length);
			
			for(error_key in formErrors)
			{				
				var inputHolder =_self.app.callInputs().getInputHolder(formHolder,error_key);					
				if(inputHolder.length)
				{
					var inputHolderTitle = _self.app.callInputs().getHolderTitle(inputHolder);					
					var inputHolderLanguage = inputHolder.attr(_self.app.callInputs().callLanguages().getAttribute());
					
					if(inputHolderTitle && errorMenue.length)
					{												
						errorMenue.find("[data-error-titles]").append(
							'<button type="button" class="dropdown-item" data-kantan-form-error="' + error_key + '">' + inputHolderTitle + '</button>'			
						);																		
						errorMenue.find("[data-kantan-form-error=" + error_key + "]").data("inputHolder",inputHolder);
						errorMenue.find("[data-kantan-form-error=" + error_key + "]").data("inputHolderLanguage",inputHolderLanguage);
						errorMenue.find("[data-kantan-form-error=" + error_key + "]").bind("click",function()
						{
							var errorScroll = $(this);
							if(typeof(errorScroll.data("inputHolderLanguage")) !== "undefined")
							{
								formHolder.find("[data-form-language-switch]").find("[data-value=" + errorScroll.data("inputHolderLanguage") + "]").trigger("click");
							}							
							_self.app.callHolders().scrollTo(kantanHolder,errorScroll.data("inputHolder"));							
						});
					}
					
					_self.setErrorToInput(
						inputHolder,
						formErrors[error_key]
					);
				}
			}
		}else
		{
			if(errorMenue.length)
			{
				errorMenue.addClass(_self.app.css_classes.hidden);
			}			
			formHolder.find("[data-error-count]").html("");
		}
	},
	
	setErrorToInput : function(inputHolder,errorMessages)
	{
		var _self = this;						
		var inputHolderErrors = inputHolder.find("[data-kantan=feedback]");				
		inputHolder.addClass(_self.css_classes.error);
		
		if(inputHolderErrors.length)
		{
			inputHolderErrors.html("");
			if(typeof(errorMessages) === "string")
			{
				inputHolderErrors.html(errorMessages);
			}
			if(typeof(errorMessages) === "object")
			{
				for(error_message in errorMessages)
				{
					inputHolderErrors.append(errorMessages[error_message]);							
				}
			}
		}				
	},
	
	removeErrorFromInput : function(inputHolder)
	{
		var _self = this;		
		var inputHolderErrors = inputHolder.find("[data-kantan=feedback]");
		inputHolder.removeClass(_self.css_classes.error);
		if(inputHolderErrors.length)
		{
			_self.resetFeedback(
				inputHolderErrors
			);
		}		
	},
	
	getValues : function(formHolder)
	{
		var formValuesArray = formHolder.serializeArray();		
		var formValues = {};
		for(formEntry in formValuesArray)
		{
			var formValue = formValuesArray[formEntry];			
			formValues[formValue.name] = formValue.value;
		}
		return formValues;
	},
	
	clearForm : function(formHolder)
	{
		formHolder.find('input:text, input:password, input:file, textarea').val('');		
		formHolder.find("select").each(function()
		{			
			$(this).val($(this).find("option:first").val());
		});
		formHolder.find("input:checkbox").each(function()
		{								
			if($(this).prop("checked"))
			{				
				$(this).parent().trigger("click");
			}
		});			
    	formHolder.find('input:radio, input:checkbox').removeAttr('checked').removeAttr('selected');    	
	}
};