/*!
* kantanjs Bundle
* https://gitlab.com/kantan-fw/kantanjs/
* Copyright (c) 2017 Ezra Neuwirth
* Licensed under the MIT license (http://www.opensource.org/licenses/mit-license.php)
* @author : Ezra Neuwirth
* @version : 1.0.0
* 
* @preserve
* Kantan.Utilities.Templates
* Creates helper Functions for the Handlebars Templates
* requires jquery/handlebars
* 
*/

Kantan.Utilities.Templates = function(currentApp)
{	
	this.app = currentApp;
};

Kantan.Utilities.Templates.prototype = 
{
	compile : function(holder,html,key)
	{
		var template = holder.find(html);		
		if(typeof(key) === "undefined")
		{
			key = "kantanTemplate";
		}		
		if(template.length)
		{			
			holder.data(
				key,
				Handlebars.compile(template.html())
			);
		}
	},
	
	load : function(holder,data,key)
	{
		if(typeof(key) === "undefined")
		{
			key = "kantanTemplate";
		}
		if(typeof(holder.data(key)) === "function")
		{
			return holder.data(key)(data);			
		}
		return "";
	},
};