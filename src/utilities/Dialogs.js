/*!
* kantanjs Bundle
* https://gitlab.com/kantan-fw/kantanjs/
* Copyright (c) 2017 Ezra Neuwirth
* Licensed under the MIT license (http://www.opensource.org/licenses/mit-license.php)
* @author : Ezra Neuwirth
* @version : 1.0.0
* 
* @preserve
* Kantan.Utilities.Dialogs
* A small Utility Class to handle Bootstrap Dialogs
* requires jquery/bootstrap
* 
*/

Kantan.Utilities.Dialogs = function(currentApp)
{	
	this.app = currentApp;
};

Kantan.Utilities.Dialogs.prototype = 
{	
	show : function(dialogHolder,moduleHolder,function_holder)
	{
		var _self = this;
		
		if(moduleHolder.length && dialogHolder.length)
		{
			_self.app.callUtility().callLoading().loading(function_holder);						
			dialogHolder.data('dialogClick',function_holder);

			switch(moduleHolder.attr("data-kantan-holder"))
			{
				case "content":
					$("body").addClass("modal-open");
				break;
				
				case "dialog":
					moduleHolder.addClass("stacked");
					_self.app.callUtility().callLoading().loading(moduleHolder);							
				break;
			}							
			dialogHolder.css("display","block").removeClass("hide").addClass("show");	
		}
	},
	
	hide : function(dialogHolder,moduleHolder,function_holder)
	{
		var _self = this;
		
		dialogHolder.removeClass("show").addClass("hide");
		setTimeout(function()
		{						
			_self.app.callUtility().callLoading().complete(dialogHolder.data('dialogClick'));
			
			switch(moduleHolder.attr("data-kantan-holder"))
			{										
				case "dialog":		
					moduleHolder.removeClass("stacked");																												
					_self.app.callUtility().callLoading().complete(moduleHolder);																								
				break;
				
				case "content":
					$("body").removeClass("modal-open");
				break;										
			}																
			dialogHolder.removeClass("hide").removeAttr("style");
		}, 300);			
	}
};