/*!
* kantanjs Bundle
* https://gitlab.com/kantan-fw/kantanjs/
* Copyright (c) 2017 Ezra Neuwirth
* Licensed under the MIT license (http://www.opensource.org/licenses/mit-license.php)
* @author : Ezra Neuwirth
* @version : 1.0.0
* 
* @preserve
* Kantan.Utilities.Loading
* Handles loading states for elements and provdies functions for adding/removing the
* css class loading defined via Kantan.App 
* requires jquery
* 
*/

Kantan.Utilities.Loading = function(currentApp)
{	
	this.app = currentApp;
};

Kantan.Utilities.Loading.prototype = 
{	
	loading : function(element)
	{		
		this.set(element,false);
	},
	
	complete : function(element)
	{	
		this.set(element,true);
	},
	
	isLoading : function(element)
	{
		var _self = this;		
		if(element.hasClass(_self.app.css_classes.loading))
		{
			return true;
		}else
		{
			return false;
		}		
	},
	
	set : function(element,remove_loading)
	{
		var _self = this;		
		
		if(typeof(element.attr("data-kantan-holder")) !== "undefined")
		{
			switch(element.attr("data-kantan-holder"))
			{
				case "dialog":
					element = element.find(".modal-content:first");
				break;
			}
		}
		
		if(remove_loading)
		{
			element.removeClass(_self.app.css_classes.loading);			
		}else
		{
			element.addClass(_self.app.css_classes.loading);				
		}
	}
};