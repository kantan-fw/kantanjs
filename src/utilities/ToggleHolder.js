/*!
* kantanjs Bundle
* https://gitlab.com/kantan-fw/kantanjs/
* Copyright (c) 2017 Ezra Neuwirth
* Licensed under the MIT license (http://www.opensource.org/licenses/mit-license.php)
* @author : Ezra Neuwirth
* @version : 1.0.0
* 
* @preserve
* Kantan.Utilities.ToggleHolder
*  Creates the toogle Functions for a holder
* requires jquery
* 
*/

Kantan.Utilities.ToggleHolder = function(currentApp)
{	
	this.app = currentApp;
};

Kantan.Utilities.ToggleHolder.prototype = 
{
	settings : {},
	
	init : function(holder,settings)
	{
		var _self = this;
		
		this.settings = this.app.callUtility().callData().extendObject(
			{							
				"states" : ["closed","open"], //the default states	
				"states-medium":["closed-medium","open-medium"],
				"states-large":["closed-large","open-large"],				
				"bodyclick" : ['open'], //which state should trigger the toggle again if the body is clicked, used to close sidebars
				"parent" : false,	//if a parent is present then it will also receive the css class from the current state				
				"scrollService" : false,	//if true the Kantan.Services.Handler().scrollTo function is called
				"animate" : false,		//you can add an class to the holder after an animation has run (still in testing)
				"breaks":	//a responsive breakpoint array, if the width of the body is greater those states will be used instead of the default 'states' array
				{					
					1233 : "states-large"
				}
			},
			settings
		);
		
		holder.on("click","[data-kantan-toggle=function]",function(event)
		{
			var function_holder = $(this);
			var target = $(event.target);
			
			_self.run(
				holder,
				function_holder
			);
		});		
	},
	
	
	run : function(holder,function_holder)
	{
		var _self = this;
		
		if(function_holder.attr("data-holder") == "service")
		{			
			var toggleHolder = _self.app.callHolders().getContentFromParent(function_holder).find("[data-kantan=" + function_holder.attr("data-toggle") + "]:first");									
		}else
		{
			var toggleHolder = holder.find("[data-kantan=" + function_holder.attr("data-toggle") + "]");	
		}
					
		if(toggleHolder.length)
		{
			if(typeof(toggleHolder.data("storedSettings")) === "object")
			{
				var toggleSettings = toggleHolder.data("storedSettings");				
				var toggleClassHolder = toggleSettings.toggleClassHolder				
			}else
			{
				var toggleSettings = _self.app.callUtility().callData().getDataValues(
		    		toggleHolder,
		    		"toggle-settings",
		    		_self.settings
		    	);
		    	
		    	var toggleClassHolder = toggleHolder;		
				if(toggleSettings.parent)
				{						
					if(toggleSettings.parent == "base")
					{
						toggleClassHolder = holder;
					}else
					{
						toggleClassHolder = toggleClassHolder.parents("[data-kantan=" + toggleSettings.parent + "]");
					}					
				}
				
				if(typeof(toggleSettings.animate) === "object")
				{			
					toggleHolder[0].addEventListener('webkitAnimationStart',function( event ) 
					{						
						 if(typeof(toggleClassHolder.attr("data-animate")) !== "undefined")
						{							
							toggleClassHolder.removeClass(toggleClassHolder.attr("data-animate"));																				
							toggleClassHolder.removeAttr("data-animate");								
						}
					});
					toggleHolder[0].addEventListener('webkitAnimationEnd',function( event ) 
					{
						var current_state = toggleClassHolder.attr("data-toggle-state");				
						if(typeof(toggleSettings.animate[current_state]) !== "undefined")
						{
							var animate_class = toggleSettings.animate[current_state];
							toggleClassHolder.addClass(animate_class);	
							toggleClassHolder.attr("data-animate",animate_class);
						}
					});
				}
				
		    	toggleSettings.toggleClassHolder = toggleClassHolder;		    	
		    	toggleHolder.data("storedSettings",toggleSettings);
			}
			
			var toggle_states_key = "states";
			if(typeof(toggleSettings.breaks) === "object")
			{
				for(break_width in toggleSettings.breaks)
				{
					var breakWidth = parseInt(break_width);				
					if(holder.width() >= breakWidth)
					{
						toggle_states_key = toggleSettings.breaks[break_width];
					}			
				}				
			}
							
			if(typeof(toggleSettings[toggle_states_key]) === "object")
			{				
				var state_found = 0;
				var set_state = 0;			
				for(state_counter in toggleSettings[toggle_states_key])
				{		
					var current_state_class = toggleSettings[toggle_states_key][state_counter];
								
					function_holder.removeClass(current_state_class);
					if(toggleSettings.parent)
					{
						toggleHolder.removeClass(current_state_class);
					}					
					if(toggleClassHolder.hasClass(current_state_class))
					{
						state_found = state_counter;													
						toggleClassHolder.removeClass(current_state_class);					
					}							
				}
				
				state_found++;			
				if(typeof(toggleSettings[toggle_states_key][state_found]) !== "undefined")
				{
					set_state = state_found;
				}else
				{
					set_state = 0;
				}				
				
				var set_state_class = toggleSettings[toggle_states_key][set_state];
				
				if(toggleSettings.scrollService && set_state > 0)
				{
					_self.app.callHolders().scrollTo(toggleClassHolder);														
				}
										
				toggleClassHolder.addClass(set_state_class);				
				toggleClassHolder.attr("data-toggle-state",set_state_class);
				
				var bind_parent_close = false;				
				if(toggleSettings.parent)
				{
					if(typeof(toggleSettings.bodyclick) === "object")
					{						
						for(parentclick_counter in toggleSettings.bodyclick)
						{							
							if(toggleSettings.bodyclick[parentclick_counter] == set_state_class)
							{
								bind_parent_close = true;								
							}
						}						
					}					
					toggleHolder.addClass(set_state_class);
				}			
				function_holder.addClass(set_state_class);
				
				if(bind_parent_close)
				{												
					$(window).bind("click.kantantoggle",function(event)
					{						
						if(event.target === toggleClassHolder[0])
						{
							function_holder.trigger("click");
						}						
					});				
				}else
				{
					$(window).unbind("click.kantantoggle");					
				}			
			}							
		}		
	}
};