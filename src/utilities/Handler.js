/*!
* kantanjs Bundle
* https://gitlab.com/kantan-fw/kantanjs/
* Copyright (c) 2017 Ezra Neuwirth
* Licensed under the MIT license (http://www.opensource.org/licenses/mit-license.php)
* @author : Ezra Neuwirth
* @version : 1.0.0
* 
* @preserve
* Kantan.Utilities.Handler
* The Handler for the Kantan.Utilitis Objects
* requires jquery
* 
*/

Kantan.Utilities.Handler = function(currentApp)
{	
	this.app = currentApp;
	
	this.objects = new Object();	
  	this.objects.toggleHolder = new Kantan.Utilities.ToggleHolder(this.app);
    this.objects.loading = new Kantan.Utilities.Loading(this.app);	    
	this.objects.forms = new Kantan.Utilities.Forms(this.app);       	    
	this.objects.data = new Kantan.Utilities.Data(this.app);		
	this.objects.templates = new Kantan.Utilities.Templates(this.app);
	this.objects.dialogs = new Kantan.Utilities.Dialogs(this.app);
	this.objects.collapse = new Kantan.Utilities.Collapse(this.app);		
	
	this.callToggleHolder = function()
	{		
		return this.objects.toggleHolder;
	};	
	this.callLoading = function()
	{		
		return this.objects.loading;
	};
	this.callData = function()
	{		
		return this.objects.data;
	};	
	this.callForms = function()
	{		
		return this.objects.forms;
	};	
	this.callTemplates = function()
	{		
		return this.objects.templates;
	};
	this.callDialogs = function()
	{		
		return this.objects.dialogs;
	};
	this.callCollapse = function()
	{		
		return this.objects.collapse;
	};	
};

Kantan.Utilities.Handler.prototype = 
{
	settings : {},
	
	init : function(holder,settings)
	{
		var _self = this;
		
		_self.settings = _self.callData().extendObject(
			{				
				"toggle" : true,
				"collapse" : true
			},
			settings
		);
		
		if(_self.settings.toggle)
		{
			_self.callToggleHolder().init(holder);	
		}
		if(_self.settings.collapse)
		{
			_self.callCollapse().init();
		}	
	}
}