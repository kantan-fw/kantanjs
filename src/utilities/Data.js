/*!
* kantanjs Bundle
* https://gitlab.com/kantan-fw/kantanjs/
* Copyright (c) 2017 Ezra Neuwirth
* Licensed under the MIT license (http://www.opensource.org/licenses/mit-license.php)
* @author : Ezra Neuwirth
* @version : 1.0.0
* 
* @preserve
* Kantan.Utilities.Data
* A small Utility Class to handle HTML data attributes, Javascript Objects and Arrays
 * requires jquery
* 
*/

Kantan.Utilities.Data = function(){};

Kantan.Utilities.Data.prototype = 
{
	objectToArray : function(object)
	{
		return Object.keys(object).map(function (key) { return object[key]; });
	},
	
	getDataValues : function(holder,key,defaultValues)
	{		
		return this.extendObject(
			defaultValues,
			holder.data(key)
		);		
	},
	
	extendObject : function(defaultValues,dataValues)
	{
		if(typeof(dataValues) !== "object")
    	{
			dataValues = {};
    	}
    	if(typeof(defaultValues) === "object")
    	{
    		if(typeof(dataValues) === "object")
    		{
    			return jQuery.extend({}, defaultValues, dataValues);    		   
    		}else
    		{
    			return defaultValues;
    		}
    	}		
    	
    	return dataValues;
	},
	
	parseDataAtrribute : function(holder,data_key)
	{		
		try {			
			return jQuery.parseJSON( holder.attr( data_key ) );					
		}catch(err) {
		   return new Object();
		}
	},
	
	init : function(holder,init_settings)
	{
		if(typeof(init_settings.data_key)  !== "undefined")
		{
			if(typeof(holder.data(init_settings.data_key)) !== "object")
			{
				holder.data(
					init_settings.data_key,
					new Object()
				);
			}
			if(typeof(init_settings.data) === "object")
			{
				holder.data(
					init_settings.data_key,
					init_settings.data
				);
			}
		}		
	},
	
	add : function(holder,add_settings)
	{	
		if(typeof(add_settings.data_key)  !== "undefined")
		{
			this.init(holder,add_settings);
			if(typeof(add_settings.key) !== "undefined")
			{				
				if(typeof(holder.data(add_settings.data_key)[add_settings.key]) !== "object")
				{
					holder.data(add_settings.data_key)[add_settings.key] = new Object();
				}			
				holder.data(add_settings.data_key)[add_settings.key][add_settings.parameter] = add_settings.value;	
			}else
			{
				holder.data(add_settings.data_key)[add_settings.parameter] = add_settings.value;	
			}			
		}		
	},
	
	changeArray : function(dataArray,value,action)
	{
		var indexValue = dataArray.indexOf(
			value
		);				
		switch(action)
		{
			case "add":
				if(indexValue == -1)
				{
					dataArray.push(value);	
				}			 	
			break;
			
			case "remove":
				if(indexValue >= 0)
				{
				 	dataArray.splice(indexValue,1);					 	
				}
			break;
		}			
	},
	
	remove : function(holder,delete_settings)
	{
		if(typeof(delete_settings.data_key)  !== "undefined")
		{
			if(typeof(delete_settings.key) !== "undefined")
			{
				if(typeof(holder.data(delete_settings.data_key)) === "object" &&
					typeof(holder.data(delete_settings.data_key)[delete_settings.key]) === "object" &&
					typeof(holder.data(delete_settings.data_key)[delete_settings.key][delete_settings.parameter]) !== "undefined")
				{
					delete holder.data(delete_settings.data_key)[delete_settings.key][delete_settings.parameter];	
				}
			}else
			{
				if(typeof(holder.data(delete_settings.data_key)[delete_settings.parameter]) !== "undefined")
				{
					delete holder.data(delete_settings.data_key)[delete_settings.parameter];	
				}
			}
		}
	},
	
	get : function(holder,get_settings)
	{
		this.init(holder,get_settings);
		
		if(typeof(get_settings.data_key)  !== "undefined")
		{
			if(typeof(get_settings.key) !== "undefined")
			{
				if(typeof(holder.data(get_settings.data_key)[get_settings.key][get_settings.parameter]) === "undefined")
				{
					if(get_settings.type !== "undefined")
					{
						switch(get_settings.type)
						{
							case "object":
								holder.data(get_settings.data_key)[get_settings.key][get_settings.parameter] = new Object();					
							break;
							
							case "array":
								holder.data(get_settings.data_key)[get_settings.key][get_settings.parameter] = new Array();
							break;
						}
					}				
				}		
				
				return holder.data(get_settings.data_key)[get_settings.key][get_settings.parameter];		
			}else
			{			
				if(typeof(holder.data(get_settings.data_key)[get_settings.parameter]) === "undefined")
				{
					if(get_settings.type !== "undefined")
					{
						switch(get_settings.type)
						{
							case "object":
								holder.data(get_settings.data_key)[get_settings.parameter] = new Object();					
							break;
							
							case "array":
								holder.data(get_settings.data_key)[get_settings.parameter] = new Array();
							break;
						}	
					}				
				}			
				return holder.data(get_settings.data_key)[get_settings.parameter];			
			}
		}		
	},
	
	clear : function(holder,clear_settings)
	{		
		if(typeof(clear_settings.data_key)  !== "undefined")
		{
			if(typeof(clear_settings.key) !== "undefined")
			{
				holder.data(clear_settings.data_key)[clear_settings.key] = new Object();	
			}else
			{
				holder.data(clear_settings.data_key) = new Object();
			}
		}		
	},
	
	getLength : function(holder,length_settings)
	{
		if(typeof(length_settings.data_key)  !== "undefined")
		{
			if(typeof(length_settings.key) === "undefined")
			{
				return Object.keys(holder.data(length_settings.data_key)).length;
			}else
			{
				return Object.keys(holder.data(length_settings.data_key)[length_settings.key]).length;
			}
		}	
	}
};