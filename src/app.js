/*!
* kantanjs Bundle
* https://gitlab.com/kantan-fw/kantanjs/
* Copyright (c) 2017 Ezra Neuwirth
* Licensed under the MIT license (http://www.opensource.org/licenses/mit-license.php)
* @author : Ezra Neuwirth
* @version : 1.0.0
* 
* @preserve
* Kantan.App
* Creates the core functions for the Kantan Admin App
* requires jquery
* for holders Kantan.Holders.Handler
* for inputs Kantan.Inputs.Handler
* for utilities Kantan.Utilities.Handler
* 
*/

Kantan.App = function()
{
	this.objects = new Object();	
	
	this.objects.utilities = new Kantan.Utilities.Handler(this);
	this.objects.holders = new Kantan.Holders.Handler(this);	       	  
    this.objects.inputs = new Kantan.Inputs.Handler(this);    
    
    this.callUtility = function()
	{		
		return this.objects.utilities;
	};
	this.callHolders = function()
	{		
		return this.objects.holders;
	};	
	this.callInputs = function()
	{		
		return this.objects.inputs;
	};	
};

Kantan.App.prototype = 
{
	holder : false,	
	
	init : function(holder,optional_css_classes)
	{
		this.holder = holder;
		
		this.css_classes = this.callUtility().callData().extendObject(
			{
				active : "active",
				disabled : "disabled",
				hidden : "d-none",
				loading : "loading"		
			},
			optional_css_classes
		);		
	}	
};