/*!
* kantanjs Bundle
* https://gitlab.com/kantan-fw/kantanjs/
* Copyright (c) 2017 Ezra Neuwirth
* Licensed under the MIT license (http://www.opensource.org/licenses/mit-license.php)
* @author : Ezra Neuwirth
* @version : 1.0.0
* 
* @preserve
* Kantan
* The Core Object from the Kantan Admin App
* 
*/

var Kantan = 
{	
	App : {},
	Holders : {},	
	Utilities : {},
	Inputs : {}
};